"""Contains various plotting functions"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

if __name__ == "__main__":
    pass


def plot_rw_mom(filepath: str, n_rw: int, timestep: float,
                show_plot: bool = False,
                savepath: str = None) -> None:
    """
    Plots reaction wheel momentum
    """

    # Define colors for plotting (max 4 wheels are expected in most cases)
    colors = ["dodgerblue", "forestgreen", "firebrick", "darkmagenta"]

    colnames = [f"Wheel {i}" for i in range(n_rw)]

    # 42 adds a white space at the end and python reads an extra column,
    # so it is required to choose as many columns as the number of RW
    rw_mom = pd.read_csv(filepath, delimiter=" ", header=None,
                         usecols=list(range(n_rw)), names=colnames)

    # construct time array
    datasize = rw_mom.shape[0]
    times = np.zeros(datasize)
    for i in range(1, datasize):
        times[i] = times[i-1] + timestep

    _, ax = plt.subplots()
    handles = []
    for i in range(len(colnames)):
        l, = ax.plot(times,
                     rw_mom[colnames[i]], label=colnames[i], c=colors[i])
        handles.append(l)

    ax.grid()
    ax.set_title("Reaction wheel angular momentum")
    ax.set_ylabel("Angular momentum [Nms]")
    ax.set_xlabel("Time [sec]")
    ax.legend(handles=handles)
    plt.tight_layout()

    if savepath is not None:
        plt.savefig(savepath, bbox_inches="tight")

    if show_plot is True:
        plt.show()


def plot_mtq_mdm(filepath: str, n_mtq: int, timestep: float,
                 show_plot: bool = False,
                 savepath: str = None) -> None:
    """
    Plots magnetorquer magnetic dipole moment
    """

    # Define colors for plotting (max 3 MTQs are expected in most cases)
    colors = ["dodgerblue", "forestgreen", "firebrick", "darkmagenta"]

    colnames = [f"MTQ {i}" for i in range(n_mtq)]

    # 42 adds a white space at the end and python reads an extra column,
    # so it is required to choose as many columns as the number of MTQs
    mtq_mdm = pd.read_csv(filepath, delimiter=" ", header=None,
                          usecols=list(range(n_mtq)), names=colnames)

    # construct time array
    datasize = mtq_mdm.shape[0]
    times = np.zeros(datasize)
    for i in range(1, datasize):
        times[i] = times[i-1] + timestep

    _, ax = plt.subplots()
    handles = []
    for i in range(len(colnames)):
        l, =  ax.plot(times, mtq_mdm[colnames[i]],
                      label=colnames[i], c=colors[i])
        handles.append(l)

    ax.grid()
    ax.set_title("Magnetorquer magnetic dipole moment")
    ax.set_ylabel("Magnetic dipole moment Am^2")
    ax.set_xlabel("Time [sec]")
    ax.legend(handles=handles)

    if savepath is not None:
        plt.savefig(savepath, bbox_inches="tight")

    if show_plot is True:
        plt.show()


def plot_euler_angles(filepath: str, timestep: float,
                      show_plot: bool = False,
                      savepath: str = None) -> None:
    """
    Plots the Euler angles timeseries
    """
    # Define colors for plotting
    colors = ["dodgerblue", "forestgreen", "firebrick", "darkmagenta"]

    colnames = ["Roll", "Pitch", "Yaw"]
    rpy = pd.read_csv(filepath, delimiter=" ", header=None, names=colnames)

    # construct time array
    datasize = rpy.shape[0]
    times = np.zeros(datasize)
    for i in range(1, datasize):
        times[i] = times[i-1] + timestep

    _, ax = plt.subplots()
    l1, = ax.plot(times, rpy["Roll"], label="Roll", c=colors[0])
    l2, = ax.plot(times, rpy["Pitch"], label="Pitch", c=colors[1])
    l3, = ax.plot(times, rpy["Yaw"], label="Yaw", c=colors[2])
    handles = [l1, l2, l3]

    ax.grid()
    ax.set_title("Euler angles in the LVLH frame")
    ax.set_ylabel("Angles [deg]")
    ax.set_xlabel("Time [sec]")
    ax.legend(handles=handles)

    if savepath is not None:
        plt.savefig(savepath, bbox_inches="tight")

    if show_plot is True:
        plt.show()


def plot_qbn(filepath: str, timestep: float,
             show_plot: bool = False,
             savepath: str = None) -> None:
    """
    Plots quaternions from the ECI to the BODY frame.
    """
    # Define colors for plotting
    colors = ["dodgerblue", "forestgreen", "firebrick", "darkmagenta"]

    colnames = ["q1", "q2", "q3", "q0"]
    quat = pd.read_csv(filepath, delimiter=" ", header=None, names=colnames)

    # construct time array
    datasize = quat.shape[0]
    times = np.zeros(datasize)
    for i in range(1, datasize):
        times[i] = times[i-1] + timestep

    _, ax = plt.subplots()
    l1, = ax.plot(times, quat["q1"], label="q1", c=colors[0])
    l2, = ax.plot(times, quat["q2"], label="q2", c=colors[1])
    l3, = ax.plot(times, quat["q3"], label="q3", c=colors[2])
    l4, = ax.plot(times, quat["q0"], label="q0-scalar", c=colors[3])
    handles = [l1, l2, l3, l4]

    ax.grid()
    ax.set_title("Quaternions from ECI to BODY frame")
    ax.set_ylabel("Quaternion")
    ax.set_xlabel("Time [sec]")
    ax.legend(handles=handles)

    if savepath is not None:
        plt.savefig(savepath, bbox_inches="tight")

    if show_plot is True:
        plt.show()


def plot_qbl(filepath: str, timestep: float,
             show_plot: bool = False,
             savepath: str = None) -> None:
    """
    Plots quaternions from the LVLH to the BODY frame.
    """
    # Define colors for plotting
    colors = ["dodgerblue", "forestgreen", "firebrick", "darkmagenta"]

    colnames = ["q1", "q2", "q3", "q0"]
    quat = pd.read_csv(filepath, delimiter=" ", header=None, names=colnames)

    # construct time array
    datasize = quat.shape[0]
    times = np.zeros(datasize)
    for i in range(1, datasize):
        times[i] = times[i-1] + timestep

    _, ax = plt.subplots()
    l1, = ax.plot(times, quat["q1"], label="q1", c=colors[0])
    l2, = ax.plot(times, quat["q2"], label="q2", c=colors[1])
    l3, = ax.plot(times, quat["q3"], label="q3", c=colors[2])
    l4, = ax.plot(times, quat["q0"], label="q0-scalar", c=colors[3])
    handles = [l1, l2, l3, l4]

    ax.grid()
    ax.set_title("Quaternions from LVLH to BODY frame")
    ax.set_ylabel("Quaternion")
    ax.set_xlabel("Time [sec]")
    ax.legend(handles=handles)

    if savepath is not None:
        plt.savefig(savepath, bbox_inches="tight")

    if show_plot is True:
        plt.show()


def plot_wbn(filepath: str, timestep: float,
             show_plot: bool = False,
             savepath: str = None) -> None:
    """
    Plots angular velocity of the BODY frame wrt the ECI frame
    """
    # Define colors for plotting
    colors = ["dodgerblue", "forestgreen", "firebrick", "darkmagenta"]

    colnames = ["omega_x", "omega_y", "omega_z"]
    omega = pd.read_csv(filepath, delimiter=" ", header=None, names=colnames)

    # construct time array
    datasize = omega.shape[0]
    times = np.zeros(datasize)
    for i in range(1, datasize):
        times[i] = times[i-1] + timestep

    _, ax = plt.subplots()
    l1, = ax.plot(times, omega["omega_x"], label="omega_x", c=colors[0])
    l2, = ax.plot(times, omega["omega_y"], label="omega_y", c=colors[1])
    l3, = ax.plot(times, omega["omega_z"], label="omega_z", c=colors[2])
    handles = [l1, l2, l3]

    ax.grid()
    ax.set_title("Angular velocities of the BODY wrt the ECI frame")
    ax.set_ylabel("Omega [rad/sec]")
    ax.set_xlabel("Time [sec]")
    ax.legend(handles=handles)

    if savepath is not None:
        plt.savefig(savepath, bbox_inches="tight")

    if show_plot is True:
        plt.show()


def plot_wbl(filepath: str, timestep: float,
             show_plot: bool = False,
             savepath: str = None) -> None:
    """
    Plots angular velocity of the BODY frame wrt the LVLH frame
    """
    # Define colors for plotting
    colors = ["dodgerblue", "forestgreen", "firebrick", "darkmagenta"]

    colnames = ["omega_x", "omega_y", "omega_z"]
    omega = pd.read_csv(filepath, delimiter=" ", header=None, names=colnames)

    # construct time array
    datasize = omega.shape[0]
    times = np.zeros(datasize)
    for i in range(1, datasize):
        times[i] = times[i-1] + timestep

    _, ax = plt.subplots()
    l1, = ax.plot(times, omega["omega_x"], label="omega_x", c=colors[0])
    l2, = ax.plot(times, omega["omega_y"], label="omega_y", c=colors[1])
    l3, = ax.plot(times, omega["omega_z"], label="omega_z", c=colors[2])
    handles = [l1, l2, l3]

    ax.grid()
    ax.set_title("Angular velocities of the BODY wrt the LVLH frame")
    ax.set_ylabel("Omega [rad/sec]")
    ax.set_xlabel("Time [sec]")
    ax.legend(handles=handles)

    if savepath is not None:
        plt.savefig(savepath, bbox_inches="tight")

    if show_plot is True:
        plt.show()
