"""Contains various utilities for mission analysis purposes"""

import os
import re
import shutil
import sys
from typing import Tuple

import numpy as np

if __name__ == "__main__":
    pass


def find_42_timestep(filepath: str) -> float:
    """Determines the file output timestep of a 42 simulation from the
    configuration files.

    :param filepath: Path to the Inp_Sim.txt file
    :type filepath: str
    :return: File output timestep
    :rtype: float
    """

    timestep = 0.0
    with open(filepath, "r", encoding="UTF-8") as input_file:
        linenum = 0
        # File output interval information is located in line 5
        for line in input_file:
            if linenum < 4:
                pass
            elif linenum == 4:
                timestep = line.split()
                timestep = float(timestep[0])
            else:
                break
            linenum += 1
    return timestep


def clean_thermal_sim_dirs(restart_dirpath: str,
                           postproc_dirpath: str, external_dt: float) -> None:
    """Cleans directories from previous thermal simulation runs.

    :param restart_dirpath: Path to the directory containing the restart files
    :type restart_dirpath: str
    :param postproc_dirpath: Path to the direcotry containing the postproc
    directories (note that the postprocessing files are within separate
    directories, one for each transient simulation)
    :type postproc_dirpath: str
    :param external_dt: Timestep between the transient simulations. It can be
    a float number, but in the current pipeline it is integer and integers are
    also convenient for file naming purposes.
    :type external_dt: float
    """

    warning_message = ("Warning: This will permanently delete all result and "
                       "postprocessing files Do you want to continue? (y/N): "
                       )

    # Prompt the user for confirmation
    user_input = input(warning_message).strip().lower()

    if user_input == "y":
        # Clear old result files if they exist
        if os.path.exists(restart_dirpath):
            for filename in os.listdir(restart_dirpath):
                if filename.endswith((".result")):
                    if (
                        (filename == restart_dirpath
                         + "out_"
                         + str(external_dt) + ".result")
                        or (filename == restart_dirpath
                            + "out_"
                            + str(external_dt) + ".result.pos")):
                        continue
                    file_path = os.path.join(restart_dirpath, filename)
                    os.remove(file_path)

        # Clear old postproc directories and files if they exist
        if os.path.exists(postproc_dirpath):
            for item in os.listdir(postproc_dirpath):
                item_path = os.path.join(postproc_dirpath, item)
                if item_path == (postproc_dirpath
                                 + "post_"
                                 + str(external_dt)
                                 + "/"):
                    continue
                if os.path.isdir(item_path):
                    shutil.rmtree(item_path)

    else:
        print("Cancelling run ...")
        sys.exit()


def change_line_in_file(file_path: str,
                        line_number: int, new_line_content: str) -> None:
    """_summary_

    :param file_path: Path to the file whose line will be changed
    :type file_path: str
    :param line_number: Number of line to be changed (0-indexed)
    :type line_number: int
    :param new_line_content: New line content, **without** without a
    newline character since one is appended at the end by this function.
    :type new_line_content: str
    """

    # Read all lines from the file
    with open(file_path, "r", encoding="UTF-8") as file:
        lines = file.readlines()

    # Check if the line number is valid
    if 0 <= line_number < len(lines):
        # Change the specified line
        lines[line_number] = new_line_content + "\n"
    else:
        print(f"Error: Line number {line_number} is out of range.")
        return

    # Write the modified lines back to the file
    with open(file_path, "w", encoding="UTF-8") as file:
        file.writelines(lines)


def vtk_id_list_to_numpy(vtk_id_list) -> np.ndarray:
    """Converts a vtk ID list to numpy

    :param vtk_id_list: VTK id list
    :return: np array corresponding to the VTK ID list
    :rtype: _type_
    """
    n = vtk_id_list.GetNumberOfIds()
    ids = [vtk_id_list.GetId(i) for i in range(n)]
    return np.array(ids)


def get_last_timestep_trans_vtu(dirpath: str) -> Tuple[str, str]:
    """Since many .vtu files are generated per transient simulation and the
    name of the last file depends on the timestep used (files are named like
    case_t00x.vtu), this function always returns the path of the file with
    the largest number in its name.

    :param dirpath: Path of the directory contaning the .vtu files
    :type dirpath: str
    :return: Name of the last file, path of the last file (joined with the
    provided path). Default name is case_t001.vtu
    :rtype: Tuple[str, str]
    """

    # Regular expression to match the file pattern
    file_pattern = re.compile(r"case_t(\d+)\.vtu")

    max_x = -1
    max_file = "case_t001.vtu"
    filename = "case_t001.vtu"

    # Iterate through the files in the directory
    for filename in os.listdir(dirpath):
        if filename.endswith(".vtu"):
            match = file_pattern.match(filename)
            if match:
                x = int(match.group(1))  # Extract the number x
                if x > max_x:
                    max_x = x
                    max_file = filename

    if max_file:
        return filename, os.path.join(dirpath, max_file)

    return None
