""" Contains classes and methods useful to represent Elmer configuration
files"""

from . import utilities


class ElmerBC:
    """
    Generic representation of an elmer boundary condition.
    """

    def __init__(self, sif_path: str,
                 idx: int, name: str, sif_line: int) -> None:

        self.m_idx = idx  # as in sif, stars from 1
        self.m_name = name
        self.m_sif_line = (
            sif_line  # Line where the title "Boundary condition X" is located
        )
        self.m_sif_path = sif_path


class HeatBC(ElmerBC):
    """
    Heat transfer boundary condition representation.
    Currently only supports heat flux input
    """

    m_heat_flux = None  # Dictionary, keys: "value" [W/m^2], "sif_line"
    m_surface_area = None

    def __init__(self, sif_path: str, idx: int, name: str, sif_line: int,
                 surface_area: float) -> None:

        super().__init__(sif_path, idx, name, sif_line)
        self.m_surface_area = surface_area

    def set_heat_flux(self, heat_flux: dict,
                      normalize_by_area: bool = False) -> None:
        """
        Sets the heat flux member.
        If the `normalize_by_area` parameter is True, it is considered that
        the input is power in W and the heat flux member value is normalized
        by area inside the function.
        """

        self.m_heat_flux = heat_flux

        if normalize_by_area is True:
            self.m_heat_flux["value"] /= self.m_surface_area

    def set_heat_flux_value(self, heat_flux_value: float,
                            normalize_by_area: bool = False) -> None:
        """
        Sets the value of the heat flux member.
        If the `normalize_by_area` parameter is True, it is considered that
        the input is power in W and the heat flux member value is normalized
        by area inside the function.
        """
        self.m_heat_flux["value"] = heat_flux_value

        if normalize_by_area is True:
            self.m_heat_flux["value"] /= self.m_surface_area

    def set_surface_area(self, surface_area: float) -> None:
        """Sets the surface area of the boundary.

        :param surface_area: Surface area
        :type surface_area: float
        """
        self.m_surface_area = surface_area

    def get_heat_flux(self) -> dict:
        """Returns the set heat flux (value and sif line).

        :return: Heat flux value and sif line
        :rtype: dict
        """
        return self.m_heat_flux

    def get_heat_flux_value(self) -> float:
        """Returns the set heat flux (value only).

        :return: Heat flux value
        :rtype: float
        """
        return self.m_heat_flux["value"]

    def get_surface_area(self) -> float:
        """Returns the boundary surface area.

        :return: Surface area
        :rtype: float
        """
        return self.m_surface_area

    def update_sif_heat_flux(self) -> None:
        """Updates the heat flux value on the sif based on the value of the
        class member.
        """
        heat_flux_line = f"  Heat Flux = {self.m_heat_flux['value']}"
        utilities.change_line_in_file(
            self.m_sif_path, self.m_heat_flux["sif_line"], heat_flux_line
        )


class ElmerSimulation:
    """
    Represents the Simulation of the Elmer solver input file
    """

    m_sif_filepath = None
    m_post_file_line = None
    m_restart_file_line = None
    m_output_file_line = None

    def __init__(self, sif_path: str, post_file_line: int,
                 restart_file_line: int, output_file_line: int) -> None:

        self.m_sif_filepath = sif_path
        self.m_post_file_line = post_file_line
        self.m_restart_file_line = restart_file_line
        self.m_output_file_line = output_file_line

    def update_sif_io_filenames(self, restart_filename: str,
                                output_filename: str,
                                postproc_filename: str) -> None:
        """
        Updates I/O file names on the Elmer sif file
        """
        restart_file_line = f'  Restart File = "{restart_filename}"'
        utilities.change_line_in_file(
            self.m_sif_filepath, self.m_restart_file_line, restart_file_line
        )

        output_file_line = f'  Output File = "{output_filename}"'
        utilities.change_line_in_file(
            self.m_sif_filepath, self.m_output_file_line, output_file_line
        )

        postproc_file_line = f"  Post File = {postproc_filename}"
        utilities.change_line_in_file(
            self.m_sif_filepath, self.m_post_file_line, postproc_file_line
        )
