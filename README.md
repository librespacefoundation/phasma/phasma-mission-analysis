# PHASMA Mission Analysis
Welcome to the mission analysis repository for the PHASMA Mission !
This is where we keep simulation configuration files, simulation scripts and tools as well as results post-processing tools.
The repository is accompanied by a python library  named `phasmalibma` which allows sharing functions among the various simulations

# Installing the python library
The first step in installing the python library that comes with this repository is creating a virtual environment and using `Python 3.10`. An easy eay to do this is using [virtualevnwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/). The `build` package is also installed in this step.
as follows:
```
mkvirtualenv -p python3.10 phasma 
workon phasma
pip install build
```

From the root directory of this project call the following commands:
```
cd phasmalibma
rm -rf dist/ phasmalibma.egg-info
python -m build
pip install dist/phasmalibma-*.tar.gz
```
Note that these commands will remove any previous version of `phasmalibma` that has been installed on the virtual environment.
Now the library has been installed in the virtual environment and can be used with
```
import phasmalibma
```
Finally, in order to get linters and autocomplete helpers to work properly, one should set the python interpreter on their editor to be the one on the virtual environment.
In VS Code, this is achieved by `Ctrl+Shift+P` > `Python:Select Interpreter` > `Enter Interpreter Path` and then choosing the `bin/python` directory of the virtual environment directory.

# Contributing to the python library
## Documentation
The `reST` docstring format is used in the python library. If using VS code, a convenient way to create docstrings is through the [autoDocstring](https://marketplace.visualstudio.com/items?itemName=njpwerner.autodocstring) extension and configuring its settings to use the `sphinx` type docstrings. 
