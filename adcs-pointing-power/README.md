# ADCS pointing power
This directory contains the necessary simulation configuration files and post-processing functions to calculate the required ADCS power during the pointing mode. Power calculation is composed of the following steps: 
1. A simulation with a pointing and a reaction wheel desaturation controller is run in 42. The simulation considers an attitude maneuver from a random initial attitude to nadir pointing, and attitude-keeping for a long time period thereafter.
2. The reaction wheel momentum is calculated by the simulator and the reaction wheel torque is calculated by numerical differentiation of the angular momentum.
3. The manufacturer (CubeSpace for the case of the PHASMA mission) provides momentum-current curves for 4 torque levels, for 2 wheel supply voltage levels (8 and 16V). In our case, the reaction wheels will be powered with 12V and the 8V curves were chosen. The curves were digitized and their numerical values can be found in `cw0017_8V_curves`.
4. For the calculated angular momentum and torque values, a double interpolation on the manufacturer's curves is performed. First, the curves are interpolated along the angular momentum axis, which results in 4 arrays containing current for each torque level. Then an interpolation along the torque "axis" is performed and the a value for the current is calculated. This value is multiplied by the supply voltage, thus giving a power timeseries.
5. Since our [power budget tool](https://gitlab.com/librespacefoundation/satellite-solar-power-budget) receives average power as input, the average of the power timeseries is calculated.
6. Steps 2, 3, 4, 5 are performed for each of the 3 wheels.


### Limitations
* The desaturation controller runs continuously, while in practice it will only run at a 10% duty cycle.
* The magnetorquer power is not considered considered.

### Notes on 42

To perform correct solar radiation pressure and aerodynamic torque calculations, one must use the 3D model of their satellite. Said 3D model must be in `.obj` form and it must be included in the `Model/` directory of 42. The .obj used for the present simulation is included in the repository.  
Moreover, the TLE or orbit data as well as the launch date (found in the `Orb_PHASMA.txt`-`TLE.txt` and `Inp_Sim.txt` files respectively) must be tuned before running the simulation.

**Note 1 :** The coordinates of the center of mass are related to the coordinate system of the CAD model. Keep this in mind in case the CM coordinates in the configuration files seem odd and make sure to verify with the aid of the graphical interface.

**Note 2 :** (making the note here because 42 does not allow notes in config files) The simulation configuration files used in the configuration files refer to the stowed configuration.
