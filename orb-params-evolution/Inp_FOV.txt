************************* Fields of View ***************************
1                                 !  Number of FOVs
--------------------------------------------------------------------
"FineSunSensor"                            !  Label
24   100.0                          !  Number of Sides, Length [m]
166.0  166.0                         !  H Width, V Height [deg]
0.7 0.7 0.0 1.0                   !  Color RGB+Alpha
WIREFRAME                         !  WIREFRAME, SOLID, VECTOR, or PLANE 
TRUE  TRUE                        !  Draw Near Field, Draw Far Field
0  0                              !  SC, Body 
0.0  0.0  0.001                     !  Position in Body [m]
0.0  0.0  -90.0  321                !  Euler Angles [deg], Sequence
Z_AXIS                            !  Boresight Axis X_AXIS, Y_AXIS, or Z_AXIS
--------------------------------------------------------------------