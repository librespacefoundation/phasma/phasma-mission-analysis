import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import astropy.time
import astropy.coordinates
from astropy import units as u
from poliastro.bodies import Earth
from poliastro.twobody import Orbit
from tqdm import tqdm

# Define colors for plotting
colors = ["dodgerblue", "forestgreen", "firebrick", "darkmagenta"]


def plot_pos_vel_ECI(
    timestep: float, pos_filename: str, vel_filename: str, plot_3d: bool = False
):
    """
    This function plots position and velocity in the ECI (J2000) frame, as exported by 42.
    """
    # ================= PLOT POSITION ===================
    colnames = ["pos_x", "pos_y", "pos_z"]
    data = pd.read_csv(pos_filename, delimiter=" ", names=colnames, dtype="float64")
    time = np.arange(0, data.shape[0] * timestep, timestep, dtype="float64")

    fig, ax = plt.subplots()
    (l1,) = ax.plot(time, data["pos_x"], label="Position X", color=colors[0])
    (l2,) = ax.plot(time, data["pos_y"], label="Position y", color=colors[1])
    (l3,) = ax.plot(time, data["pos_z"], label="Position z", color=colors[2])
    ax.set_title("Position from 42 in the ECI (J2000) frame")
    ax.legend(handles=[l1, l2, l3])

    # ================= 3D PLOT ===================
    if plot_3d is True:
        fig = plt.figure(figsize=(10, 10))
        ax = plt.axes(projection="3d")
        ax.plot3D(data["pos_x"], data["pos_y"], data["pos_z"])
        ax.set_xlabel("X ECI")
        ax.set_ylabel("Y ECI")
        ax.set_zlabel("Z ECI")

    # ================= PLOT VELOCITY ===================
    colnames = ["vel_x", "vel_y", "vel_z"]
    data = pd.read_csv(vel_filename, delimiter=" ", names=colnames, dtype="float64")
    time = np.arange(0, data.shape[0] * timestep, timestep, dtype="float64")

    fig, ax = plt.subplots()
    (l1,) = ax.plot(time, data["vel_x"], label="Velocity X", color=colors[0])
    (l2,) = ax.plot(time, data["vel_y"], label="Velocity y", color=colors[1])
    (l3,) = ax.plot(time, data["vel_z"], label="Velocity z", color=colors[2])

    ax.set_title("Velocity from 42 in the ECI (J2000) frame")
    ax.legend(handles=[l1, l2, l3])

    plt.show()


def plot_orb_params(
    orb_params: np.ndarray,
    timescale: str = "sec",
    plot_nu: bool = True,
    time: np.ndarray = 0,
    show_plot: bool = False,
):
    """
    Plots orbital parameters
    Params:
        - orb_params: Orbital parameters data. Shape : n_points x n_parameters
        - timescale: Units for the time axis. Can be 'sec', 'days', 'weeks', 'months'.
        - plot_nu: Plots the true anomaly
        - time: Array of times. If 0 (default), the function considers the 1st column of the provided file to contain time
        - show_plot: Show or just create the plot
    """

    # Select time as the first column of the provided csv file
    if not np.isscalar(time):
        oe = orb_params
    else:
        time = orb_params[:, 0]
        oe = orb_params[:, 1:]

    # Adjust time scale
    if timescale == "sec":
        pass
    elif timescale == "days":
        time /= 24 * 3600
    elif timescale == "weeks":
        time /= 7 * 24 * 3600
    elif timescale == "months":
        time /= 12 * 7 * 24 * 3600

    fig1, ((ax0, ax1), (ax2, ax3), (ax4, ax5)) = plt.subplots(3, 2, figsize=(11, 9))

    if plot_nu is False:
        fig1.delaxes(ax5)  # The indexing is zero-based here

    ax0.plot(time, oe[:, 0])
    ax0.set_xlabel("Time [" + timescale + "]")
    ax0.set_ylabel("$a [m]$")
    ax0.grid(linestyle="--", alpha=0.7)

    ax1.plot(time, oe[:, 1])
    ax1.set_xlabel("Time [" + timescale + "]")
    ax1.set_ylabel("$e []$")
    ax1.grid(linestyle="--", alpha=0.7)

    ax2.plot(time, np.rad2deg(oe[:, 2]))
    ax2.set_xlabel("Time [" + timescale + "]")
    ax2.set_ylabel("$i [deg]$")
    ax2.grid(linestyle="--", alpha=0.7)

    ax3.plot(time, np.rad2deg(oe[:, 3]))
    ax3.set_xlabel("Time [" + timescale + "]")
    ax3.set_ylabel("$RAAN [deg]$")
    ax3.grid(linestyle="--", alpha=0.7)

    ax4.plot(time, np.rad2deg(oe[:, 4]))
    ax4.set_xlabel("Time [" + timescale + "]")
    ax4.set_ylabel("$argp [deg]$")
    ax4.grid(linestyle="--", alpha=0.7)

    if plot_nu:
        ax5.plot(time, np.rad2deg(oe[:, 5]))
        ax5.set_xlabel("Time [" + timescale + "]")
        ax5.set_ylabel("$f [deg]$")
        ax5.grid(linestyle="--", alpha=0.7)

    plt.suptitle("Orbital Parameters Evolution")
    plt.tight_layout()

    if show_plot:
        plt.show()


def calc_beta_angle(
    JD: float, raan: float, i: float, eps: float = np.deg2rad(23.45)
) -> float:
    """
    Calculates the beta angle at a given date, for a given orbit.
    Params:
        - JD: Julian Day
        - raan: Right Ascension of ascending node [rad]
        - i: Inclination [rad]
        - eps: obliquity of the ecliptic (approx 23.45 deg)
    """

    # Calculate ecliptic true solar longitude with astropy
    time = astropy.time.Time(JD, format="jd")
    sun = astropy.coordinates.get_body("sun", time=time)
    frame = astropy.coordinates.GeocentricTrueEcliptic(equinox=time)
    Ls = sun.transform_to(frame).lon.value  # [deg]
    Ls = np.deg2rad(Ls)

    beta = np.arcsin(
        np.cos(Ls) * np.sin(raan) * np.sin(i)
        - np.sin(Ls) * np.cos(eps) * np.cos(raan) * np.sin(i)
        + np.sin(Ls) * np.sin(eps) * np.cos(i)
    )  # [rad]

    return beta


def find_timestep_42(simul_dir: str) -> float:
    """
    Determines the file output timestep of 42 by parsing the InpSim.txt config file.
    """
    # read the timestep (file output timestep) from the Inp_sim file
    inp_sim_path = simul_dir + "Inp_Sim.txt"
    with open(inp_sim_path, "r") as input_file:
        linenum = 0
        # File output interval information is located in line 5
        for line in input_file:
            if linenum < 4:
                pass
            elif linenum == 4:
                timestep = line.split()
                timestep = float(timestep[0])
                break
            linenum += 1

    return timestep


def RV_to_oscOE_poliastro(r: np.ndarray, v: np.ndarray) -> np.ndarray:
    """
    Converts position and velocity data to osculating orbital elements.
    The semi-major axis is returned in [m].
    Params:
        - r: Position vector. np.ndarray with astropy.units
        - v: Velocity vector. np.ndarray with astropy.units

    """
    orbit = Orbit.from_vectors(Earth, r, v)
    elements = orbit.classical()  # [a, e, i, raan, argp, nu]
    elements_array = np.array(
        [
            elements[0]
            .to(u.m)
            .value,  # poliastro returns a in km regardless of input units
            elements[1].value,
            np.deg2rad(elements[2].value),
            np.deg2rad(elements[3].value),
            np.deg2rad(elements[4].value),
            np.deg2rad(elements[5].value),
        ]
    )
    return elements_array


def RV_to_oscOE(pos: np.ndarray, vel: np.ndarray, miu_earth: float) -> np.ndarray:
    """
    A custom implementation of RV to oscOE conversion.
    Runs faster than poliastro because it does not create an `Orbit` object
    and it accepts nx3 arrays as inputs to process multiple simulation
    timesteps at once.
    Params:
        - r: Position vector array. Shape: nx3
        - v: Velocity vector array. Shape: nx3
    """

    h = np.cross(pos, vel)
    Khat = np.array([[0, 0, 1]] * pos.shape[0])
    n = np.cross(Khat, h)
    vel_norm_sq = np.linalg.norm(vel, axis=1) ** 2  # Square of the norm of velocity
    pos_norm = np.linalg.norm(pos, axis=1)  # Norm of position
    ecc = (
        (vel_norm_sq - miu_earth / pos_norm)[:, None] * pos
        - np.sum(pos * vel, axis=1)[:, None] * vel
    ) / miu_earth

    E = np.linalg.norm(vel, axis=1) ** 2 / 2 - miu_earth / np.linalg.norm(pos, axis=1)

    # Compute semi-major axis (alpha)
    alpha = -miu_earth / (2 * E)
    h = np.cross(pos, vel)
    inc = np.arccos(h[:, 2] / np.linalg.norm(h, axis=1))
    Khat = np.array([[0, 0, 1]] * pos.shape[0])
    n = np.cross(Khat, h)
    raan = np.arccos(n[:, 0] / np.linalg.norm(n, axis=1))
    ecc = (
        (np.linalg.norm(vel, axis=1) ** 2 - miu_earth / np.linalg.norm(pos, axis=1))[
            :, None
        ]
        * pos
        - np.sum(pos * vel, axis=1)[:, None] * vel
    ) / miu_earth
    omega = np.arccos(
        np.sum(n * ecc, axis=1)
        / (np.linalg.norm(n, axis=1) * np.linalg.norm(ecc, axis=1))
    )
    f = np.arccos(
        np.sum(ecc * pos, axis=1)
        / (np.linalg.norm(ecc, axis=1) * np.linalg.norm(pos, axis=1))
    )

    for i in range(len(raan)):
        # check and adjust raan
        if n[i, 1] < 0:
            raan[i] = 2 * np.pi - raan[i]

        # Check and adjust omega
        if ecc[i, 2] < 0:
            omega[i] = 2 * np.pi - omega[i]

        # Check and adjust f
        if np.sum(pos[i] * vel[i]) < 0:
            f[i] = 2 * np.pi - f[i]

    result_array = np.stack(
        (alpha, np.linalg.norm(ecc, axis=1), inc, raan, omega, f), axis=1
    )
    return result_array


def running_integral(
    running_itg: float | np.ndarray,
    last_term: float | np.ndarray,
    next_term: float | np.ndarray,
    first_term: float | np.ndarray,
    second_term: float | np.ndarray,
    dt: float,
    integration_period: float,
) -> float:
    """
    Computes a running integral considering elements spaced apart by dt, for a given integration period.
    Params:
        - first_term: The first element in the integration period
        - second_term: The second element in the integration period
        - last_term: The last element in the integration period
        - next_term: The element to be added to the running integral
    """
    # Remove the contribution of the first term and add the contribution of the incoming term
    running_itg += (last_term + next_term) * dt / 2.0 - (
        first_term + second_term
    ) * dt / 2.0

    return running_itg


def oscOE_to_MOE(
    oscoe: np.ndarray,
    orb_period: float,
    time_col: int = 0,
) -> np.ndarray:
    """
    Converts osculating orbital elements to mean orbital elements by integrating over an orbit.
    Params:
        - oscoe: [time, a, e, i, raan, argp, nu]
        - orb_period: Orbital period. In the same units as time in oscoe
        - time_col: Index of the column of oscoe that contains time
    Returns:
        - An array of the same ncols as oscoe with the time and true anomaly
        unmodified and the other elements being orbit-averaged.
    """
    # Initiate a list of running integrals (equal in size to the to-be-integrated elements)
    running_itg = np.zeros(oscoe.shape[1] - 1 - 1)  # time and nu are excluded
    running_sum = np.zeros(oscoe.shape[1] - 1 - 1)  # time and nu are excluded

    # Determine timestep from the difference of the first 2 elements of the time column
    timestep = oscoe[1, time_col] - oscoe[0, time_col]
    cutoff_idx = int(orb_period / timestep)  # +- 1 row does not make a difference

    # Initiate an array to store mean elements
    MOE = np.zeros([oscoe.shape[0] - cutoff_idx, oscoe.shape[1]])

    for i in tqdm(range(oscoe.shape[0]), desc="Converting osculating to mean OE "):
        # Fill in the integral over the first orbit
        # if i > 0 and i < cutoff_idx:
        #     running_itg += (oscoe[i, 1:-1] + oscoe[i - 1, 1:-1]) * timestep / 2.0

        if i < cutoff_idx:
            running_sum += oscoe[i, 1:-1]

        # Calculate the MOE as a moving average of OSCOE over an orbit
        if i >= cutoff_idx:
            # running_itg = running_integral(
            #     running_itg,
            #     oscoe[i - 1, 1:-1],
            #     oscoe[i, 1:-1],
            #     oscoe[i - cutoff_idx, 1:-1],
            #     oscoe[i - cutoff_idx + 1, 1:-1],
            #     timestep,
            #     orb_period,
            # ) / orb_period

            running_sum = running_sum - oscoe[i - cutoff_idx, 1:-1] + oscoe[i, 1:-1]
            running_avg = running_sum / cutoff_idx

            MOE[i - cutoff_idx, 1:-1] = running_avg

    # copy nu and time "as-is"
    MOE[:, 0] = oscoe[cutoff_idx:, 0]
    MOE[:, -1] = oscoe[cutoff_idx:, -1]

    return MOE


if __name__ == "__main__":
    pass
