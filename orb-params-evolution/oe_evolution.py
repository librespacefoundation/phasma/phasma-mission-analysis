from utilities import *
import numpy as np
from astropy.time import Time, TimeDelta
import astropy.units as u
from tqdm import tqdm
import pandas as pd
import os

if __name__ == "__main__":

    simul_dir = "orb-params-evolution/"
    postproc_results_dir = "orb-params-evolution/results/"
    pos_filename = postproc_results_dir + "PosN.42"
    vel_filename = postproc_results_dir + "VelN.42"
    # plot_pos_vel_ECI(timestep, pos_filename, vel_filename, plot_3d=True)

    if not os.path.exists(postproc_results_dir):
        os.makedirs(postproc_results_dir)

    # Determine simulation timestep (file logging timestep)
    timestep = find_timestep_42(simul_dir)

    # Read simulation RV results
    pos_colnames = ["x_eci", "y_eci", "z_eci"]
    pos_df = pd.read_csv(
        pos_filename,
        delimiter=" ",
        header=None,
        names=pos_colnames,
        usecols=[0, 1, 2],
        dtype="float64",
    )
    pos = np.array(pos_df)

    vel_colnames = ["xdot_eci", "ydot_eci", "zdot_eci"]
    vel_df = pd.read_csv(
        vel_filename,
        delimiter=" ",
        header=None,
        names=vel_colnames,
        usecols=[0, 1, 2],
        dtype="float64",
    )
    vel = np.array(vel_df)

    # Calculate osculating orbital elements evolution
    miu_earth = 3.986004418e14  # [m^3/s^2]
    oe = RV_to_oscOE(pos, vel, miu_earth)

    # Create an array with all timepoints
    full_times = np.arange(0, oe.shape[0] * timestep, timestep)

    # Downsample the OE array
    sampling_step = 1000
    oe_sampled = oe[::sampling_step]

    # Combine oe and full times in the same array
    oe = np.stack(
        (full_times, oe[:, 0], oe[:, 1], oe[:, 2], oe[:, 3], oe[:, 4], oe[:, 5]), axis=1
    )

    # Convert osculating oe to moe
    orb_period = 5600  # [sec]
    moe = oscOE_to_MOE(oe, orb_period, 0)
    moe_sampled = moe[::sampling_step]

    # Create an array of Julian days and times in sec after simulation init
    idx_array = np.arange(0, moe.shape[0], sampling_step)
    times = timestep * idx_array

    jd_times = np.zeros(times.shape[0])
    iso_utc_start_time = Time("2023-06-12T22:36:10.0", format="isot", scale="utc")
    timestep_astropy = TimeDelta((sampling_step * timestep) * u.s, format="sec").jd
    jd_times[0] = iso_utc_start_time.jd

    # Caclulate beta angle evolution
    beta = np.zeros(times.shape[0])  # [a, e, i, raan, argp, nu, beta]
    beta[0] = calc_beta_angle(jd_times[0], moe_sampled[0, 4], moe_sampled[0, 3])

    # Run the beta angle calculation loop
    for i in tqdm(range(1, times.shape[0]), desc="Calcualting beta angle evolution "):
        # Calculate new times
        jd_times[i] = jd_times[i - 1] + timestep_astropy

        # Calculate new beta angle
        beta[i] = calc_beta_angle(jd_times[i], moe_sampled[i, 4], moe_sampled[i, 3])

    # Combine beta angle and sampled times
    beta = np.stack((times, beta), axis=1)

    # Save to csv files
    oscoe_colnames = ["time", "a", "e", "i", "raan", "argp", "nu"]
    oscoe_df = pd.DataFrame(oe, columns=oscoe_colnames)
    oscoe_df.to_csv(postproc_results_dir + "oscoe_evolution.csv", index=False)

    moscoe_colnames = ["time", "a", "e", "i", "raan", "argp", "nu"]
    moscoe_df = pd.DataFrame(moe, columns=moscoe_colnames)
    moscoe_df.to_csv(postproc_results_dir + "moe_evolution.csv", index=False)

    beta_colnames = ["time", "beta"]
    beta_df = pd.DataFrame(beta, columns=beta_colnames)
    beta_df.to_csv(postproc_results_dir + "beta_evolution.csv", index=False)
