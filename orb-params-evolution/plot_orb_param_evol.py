import numpy as np
from utilities import *

if __name__ == "__main__":
    simul_dir = "orb-params-evolution/"
    oscoe_file = simul_dir + "results/oscoe_evolution.csv"
    moe_file = simul_dir + "results/moe_evolution.csv"
    beta_file = simul_dir + "results/beta_evolution.csv"
    timestep = find_timestep_42(simul_dir)

    # Read osculating orbital parameters
    oscoe = pd.read_csv(oscoe_file, header=0)
    oscoe = np.array(oscoe)

    # Read mean orbital parameters
    moe = pd.read_csv(moe_file, header=0)
    moe = np.array(moe)

    # Read beta angles
    beta = pd.read_csv(beta_file, header=0)
    beta = np.array(beta)

    fig, ax = plt.subplots()
    ax.plot(beta[:, 0] / (7 * 24 * 3600), np.rad2deg(beta[:, 1]))
    ax.set_xlabel("Time [weeks]")
    ax.set_ylabel("beta [deg]")
    ax.grid(linestyle="--", alpha=0.7)
    ax.set_title("Beta angle evolution over 1 year")

    plot_orb_params(moe, timescale="weeks", plot_nu=False)
    plot_orb_params(oscoe, timescale="weeks", plot_nu=False)

    plt.show()
