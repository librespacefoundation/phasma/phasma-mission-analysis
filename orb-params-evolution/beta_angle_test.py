import numpy as np
from astropy.time import Time, TimeDelta
import astropy.units as u
from utilities import calc_beta_angle

# iso_utc_time = Time("2023-06-12T22:36:10.0", format="isot", scale="utc")
iso_utc_time = Time("2024-06-12T22:36:10.0", format="isot", scale="utc")
jd_time = iso_utc_time.jd

raan = 4.84 # [rad]
i = np.deg2rad(91.7) # [rad]

beta = calc_beta_angle(jd_time, raan, i)
print(np.rad2deg(beta))