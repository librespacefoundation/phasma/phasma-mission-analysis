# Long-term parameters evolution simulation
A simulation to determine the long-term (1 year) evolution of the orbital parameters (mean orbital elements) and the beta angle.

> The simulation takes a long time to run doe to the small timestep required for numerical stability and the long propagation duration.

Due to their large size, the position-velocity simulation results are not saved on GitLab and can be found [here](https://cloud.libre.space/s/EmbkTQA9jp9QMt2), together with the simulation configuration files that produced them and under the `<milestone>-<revision>` naming convention. So, `IR-1` means the 1st simulation that was run during the IR milestone. For the rest of the results (beta angle and mean orbital elements evolution), the user can quickly generate them bu running the scripts in this repository.

To perform correct solar radiation pressure and aerodynamic torque calculations, one must use the 3D model of their satellite. Said 3D model must be in `.obj` form and it must be included in the `Model/` directory of 42. The .obj used for the present simulation is included in the repository.  
Moreover, the TLE or orbit data as well as the launch date (found in the `Orb_PASMA.txt`-`TLE.txt` and `Inp_Sim.txt` files respectively) must be tuned before running the simulation.

**Note 1 :** The coordinates of the center of mass are related to the coordinate system of the CAD model. Keep this in mind in case the CM coordinates in the configuration files seem odd and make sure to verify with the aid of the graphical interface.

**Note 2 :** (making the note here because 42 does not allow notes in config files) The simulation configuration files used in the configuration files refer to the stowed configuration. They will be updated once the moments of inertia for the deployed satellite are calculated.