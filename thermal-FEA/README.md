# Thermal FEA
This directory contains Finite Element Analysis simulations regarding the thermal behavior of the satellite over the course of an orbit. Simulations are performed using: 
- Any CAD tool for model generation, as long as it supports STEP export.
- [Salome-Meca](https://code-aster.org/spip.php?article303) for meshing
- [Elmer FEM](http://www.elmerfem.org/blog/) for FEA
- Custom python functions (contained in `phasmalibma`) to manipulate Elmer config files for thermal simulations

## Minimum working example
To avoid experimenting with the full model, a minimum working example has been created with all the necessary features:
- Complete mesh
- Conductive heat transfer
- Heat flux input from the Sun (+albedo and IR) and the PCBs
- Inter-component radiative heat transfer
- Radiative hear transfer to the space environment

The structure of the `min_working_example` directory has as follows:
- `steady_state` : Contains a steady state simulation
- `point_transient` : Contains a transient simulation for fixed boundary conditions (i.e. as if it was performed for a single point of an orbit)
- `orbit_transient` : Contains a series of transient simulations, each starting with different boundary conditions and using the final state of the transient simulation of the previous time step as initial conditions.

# Orbit transient simulations
Orbit transient simulations are constructed on the basis of the results of a transient simulation as the initial conditions of the next transient simulation. The "outer" timestep is the same as the one used in the heat flux calculation, while the inner timestep can be chosen as a compromise between fidelity and computational cost; an inner timestep of 10s and an outer timestep of 60s were used in PHASMA IR simulations.

Three types of files are distinguished in this pipeline:
* Output files: Are the files where simulation results are written in a format that can be used to initiate another simulation. The user can choose between a binary and a text format for this file, with the binary format requiring less space.
* Restart files: These are just filenames, which refer to the output files to be used in order to initiate a simulation. Naturally in orbit transient simulations the restart files will refer to a timestep before that of the output files.
* Postproc files: Mesh files used for postprocessing. For the present simulation pipeline the VTU format has been chosen.

## Creating the computational mesh
Considering the a 3D STEP model has been created in any CAD software, the user can import said model into Salome-Meca in order to create the computational mesh. The following general instructions are provided for the meshing process:
1. In the Geometry module of Salome-Meca perform the `Partition` operation on the imported body.
1. On the object resulting from the Partition operation (named just "geometry model" from now on) create a group for each of the desired thermal analysis bodies.
    1. Start by the solid groups and once these are created, move on to the surface groups. Surface groups are required to set boundary conditions (they will be the Elmer-recognized boundaries).

    1. Since Salome-Meca will have to recognize a lot of surfaces each time the process is very laggy. For that purpose, one can select the surface groups not from the whole geometrical model, but using a second body as tool and selecting the option to only show surfaces of the second body.
1. After creating all geometry groups, one can create the computational mesh from the Mesh module in Salome-Meca.
    1. The NETGEN-1D-2D-3D algorithm with default settings works fine.
    2. Shall any errors arise during the meshing process, check for intersecting faces in the CAD model and for very small faces - possibly in manufacturer-provided STEP files.
    3. In general, a model clean-up is suggested and, even better, the creation of a simplified CAD model in the native format of the selected CAD tool. This will make assembly much easier, resulting in far less meshing errors due to intersecting faces.
1. Save the mesh as `.unv` file.
1. Run ElmerGrid on the created mesh as:

        ElmerGrid 8 2 Mesh_name.unv -autoclean
    1. Regarding troubleshooting of this procedure, the only problem that was identified in our cases was that some surface group names had not been generated properly if the order of creation of body and surface groups was mixed. This was solved by selecting the "sort children" option after right-clicking the geometry model in the working tree of the Geometry module of Salome-Meca and then re-generating the mesh.

## Configuring the simulation
### Elmer sif configuration
First and foremost one has to be able to configure body properties as well as boundary and initial conditions. For that, associating body and boundary numbers with names is of vital importance. Said association can be found in the `mesh.names` file, which is generated after running ElmerGrid on the unv mesh file. One can then manually write the sif file following one of the sif files provided in this or in Elmer repository as an example.

### Python script configuration
The method used to run orbit transient simulations relies on a-priori knowledge of the boundary condition line numbers in order to modify the sif according to the conditions of each timestep. For this purpose, the script must be configured with the line numbers at which the necessary boundary conditions and result/postproc files are located. The following have to be configured in the script:

1. The lines where the restart filename, the output filename and the postproc filename are located. These are provided when creating an `ElmerSimulation` object as 

          sif_simulation = ElmerSimulation(sif_filename, post_file_line, restart_file_line, output_file_line)

1. The lines where the heat input of the boundary conditions is located. These are provided when creating a `HeatBC` object as 

        heat_mx_HDRM = HeatBC(sif_filename:str, idx:int, name:str, line_num:int, surface_area:float)

    The `idx` and `name` arguments are arbitrary and are bound for deprecation. The `surface_area` argument is used only when one wants to provide the heat instead of the heat flux in the Python layer. In this case, the `normalize_by_area` argument must be set to `True` in the `set_heat_flux_value` function.


## Running the simulation

### First simulation
Note that the first simulation has no previous simulation to get its initial conditions from. Consequently, the user must configure it and run it beforehand. The goal is to automate this process into the python script, but for now the user must create the appropriate result directories for the first simulation and run it before running the script. More specifically, the user must perform the following actions on the `.sif` file:
1. On the initial file, in the simulation section, add only

        Output File = "<whatever_name_you_want>.result"
        Binary Output = True
    or comment out the lines related to the restart files (commenting is done using a `!` in the beginning of the line)

2. Run the simulation as:

        ElmerSolver sif_name.sif

3. On the previous `.sif` file, append the following right above the output file lines

        Restart File = "<whatever_name_you_gave_to_the_previous_result_file>.result"
        Restart Position = 0

### Subsequent simulations
To run the subsequent orbit transient simulations, just configure the directories and boundary condition lines in `run_orbit_transient.py` and run the script.


## Visualizing body temperature evolution
The results from the orbit transient simulation are saved in the following structure:
```
Letters x, y, z represent numbers in increasing order and might be more than 3, depending on the step of the sub-transient simulations. For an orbit timestep of 60s, file naming will be as follows:

postproc/
├─ post_60/
│  ├─ case_x.vtu
│  ├─ case_y.vtu
│  ├─ case_z.vtu
├─ post_120/
│  ├─ case_x.vtu
│  ├─ case_y.vtu
│  ├─ case_z.vtu
```

The `result_compiler.py` script iterates through the result directories, opens the latest `.vtu` file from each timestep and compiles the min, max, and mean temperatures of all bodies into csv files named as `body_x.csv`. The bodies whose temperatures are compiled are selected using their number IDs in the script and the number-to-body association can be found in the `mesh.names` file.


The `plot_body_temperature.py` script reads the csv files produced by the result compiler and provides the time-series plot of a body's mean temperature. The bodies to be plotted are selected using their number IDs in the script, and again the number-to-body association can be found in the `mesh.names` file.
