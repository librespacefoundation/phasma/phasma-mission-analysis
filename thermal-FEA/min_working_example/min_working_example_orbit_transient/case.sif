Header
  CHECK KEYWORDS Warn
  Mesh DB "." "."
  Include Path ""
  Results Directory ""
End

Simulation
  Max Output Level = 5
  Coordinate System = Cartesian
  Simulation Type = Transient
  Steady State Max Iterations = 1
  Output Intervals(1) = 10
  Timestep intervals(1) = 30
  Timestep Sizes(1) = 2
  Timestepping Method = BDF
  BDF Order = 1
  Solver Input File = case.sif
  Post File = postproc/post_51120/case.vtu
  Restart File = "restart_files/out_51060.result"
  Restart Position = 0
  Output File = "restart_files/out_51120.result"
  Binary Output = True
End

Constants
  Gravity(4) = 0 -1 0 9.82
  Stefan Boltzmann = 5.670374419e-08
  Permittivity of Vacuum = 8.85418781e-12
  Permeability of Vacuum = 1.25663706e-6
  Boltzmann Constant = 1.380649e-23
  Unit Charge = 1.6021766e-19
End

Body 1
  Target Bodies(1) = 1
  Name = "Body 1"
  Equation = 1
  Material = 1
  Initial condition = 1
End

Body 2
  Target Bodies(1) = 2
  Name = "Body 2"
  Equation = 1
  Material = 1
  Initial condition = 1
End

Body 3
  Target Bodies(1) = 3
  Name = "Body Property 3"
  Equation = 1
  Material = 1
  Initial condition = 1
End

Body 4
  Target Bodies(1) = 4
  Name = "Body 4"
  Equation = 1
  Material = 1
  Initial condition = 1
End

Body 5
  Target Bodies(1) = 5
  Name = "Body 5"
  Equation = 1
  Material = 1
  Initial condition = 1
End

Solver 1
  Equation = Heat Equation
  Calculate Loads = True
  Variable = Temperature
  Procedure = "HeatSolve" "HeatSolver"
  Exec Solver = After Timestep
  Stabilize = True
  Optimize Bandwidth = True
  Steady State Convergence Tolerance = 1.0e-5
  Nonlinear System Convergence Tolerance = 1.0e-9
  Nonlinear System Max Iterations = 50
  Nonlinear System Newton After Iterations = 3
  Nonlinear System Newton After Tolerance = 1.0e-4
  Nonlinear System Relaxation Factor = 0.6
  Linear System Solver = Iterative
  Linear System Iterative Method = BiCGStab
  Linear System Max Iterations = 500
  Linear System Convergence Tolerance = 1.0e-10
  BiCGstabl polynomial degree = 2
  Linear System Preconditioning = ILU3
  Linear System ILUT Tolerance = 1.0e-3
  Linear System Abort Not Converged = False
  Linear System Residual Output = 10
  Linear System Precondition Recompute = 1
End

Equation 1
  Name = "Heat"
  Active Solvers(1) = 1
End

Material 1
  Name = "Aluminium (generic)"
  Heat Conductivity = 237.0
  Heat Capacity = 897.0
  Youngs modulus = 70.0e9
  Sound speed = 5000.0
  Poisson ratio = 0.35
  Heat expansion Coefficient = 23.1e-6
  Density = 2700.0
End

Material 2
  Name = "Polyvinyl chloride (generic)"
  Youngs modulus = 3100.0e6
  Heat expansion Coefficient = 80.0e-6
  Density = 1380.0
  Poisson ratio = 0.41
  Heat Capacity = 900.0
  Heat Conductivity = 0.16
  Emissivity = 0.0
End

Initial Condition 1
  Name = "Init temp"
  Temperature = 300
End

Boundary Condition 1
  Target Boundaries(5) = 6 14 15 17 25 
  Name = "Radiation_Gray"
  Radiation = Diffuse Gray
  Radiation Boundary Open = True
  Emissivity = 0.6
  External Temperature = 15
End

Boundary Condition 2
  Target Boundaries(1) = 2 
  Name = "qin_zm"
  Radiation = Idealized
  Emissivity = 0.6
  External Temperature = 15.0
  Heat Flux = 1041.2902859774633
End

Boundary Condition 3
  Target Boundaries(1) = 16 
  Name = "pcb_heating"
  External Temperature = 15
  Radiation = Diffuse Gray
  Radiation Boundary Open = True
  Heat Flux = 1100.0
  Emissivity = 0.6
End

Boundary Condition 4
  Target Boundaries(1) = 29 
  Name = "qin_zp"
  Heat Flux = 11.449649146322708
  Radiation = Idealized
  Emissivity = 0.6
  External Temperature = 15
End

Boundary Condition 5
  Target Boundaries(1) = 18 
  Name = "qin_xp"
  Heat Flux = 17.581954135824574
  External Temperature = 15
  Emissivity = 0.6
  Radiation = Idealized
End

Boundary Condition 6
  Target Boundaries(1) = 13 
  Name = "qin_xm"
  Heat Flux = 52.57384220446168
  Emissivity = 0.6
  Radiation = Idealized
  External Temperature = 15
End
