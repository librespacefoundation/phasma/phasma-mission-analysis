import os
import shutil
import sys

class ElmerBC():
    """
    Generic representation of an elmer boundary condition.
    """
    def __init__(self, idx:int, name:str, sif_line:int) -> None:
        self.m_idx = idx # as in sif, stars from 1
        self.m_name = name
        self.m_sif_line = sif_line # Line where the title "Boundary condition X" is located


class HeatBC(ElmerBC):
    """
    Heat transfer boundary condition representation.
    Currently only supports heat flux input
    """
    m_heat_flux = None

    def __init__(self, idx: int, name: str, sif_line:int) -> None:
        super().__init__(idx, name, sif_line)

    def set_heat_flux(self, heat_flux:dict)->None:
        self.m_heat_flux = heat_flux # keys: "value", "sif_line"
    
    def set_heat_flux_value(self, heat_flux_value:float)->None:
        self.m_heat_flux["value"] = heat_flux_value

    def get_heat_flux(self)->dict:
        return self.m_heat_flux
    
    def get_heat_flux_value(self)->float:
        return self.m_heat_flux["value"]


class ElmerSif():
    def __init__(self, path) -> None:
        self.m_path = path
     

def cleanup_dirs(results_dirpath:str, postproc_dirpath:str)->None:
    warning_message = (
    f"Warning: This will permanently delete all result and postprocessing files "
    "Do you want to continue? (y/N): "
    )

    # Prompt the user for confirmation
    user_input = input(warning_message).strip().lower()

    if user_input == 'y':    
        # Create result files and postproc files directories if they do not exist
        if not os.path.exists(results_dirpath):
            os.makedirs(results_dirpath)
            os.makedirs(postproc_dirpath)

        # Clear old result files if they exist
        for filename in os.listdir(results_dirpath):
            if filename.endswith(('.result')):
                file_path = os.path.join(results_dirpath, filename)
                os.remove(file_path)

        # Clear old postproc directories and files if they exist
        for item in os.listdir(postproc_dirpath):
            item_path = os.path.join(postproc_dirpath, item)
            if os.path.isdir(item_path):
                shutil.rmtree(item_path)
    
    else:
        print("Cancelling run ...")
        sys.exit()


def change_line_in_file(file_path:str, line_number:int, new_line_content:str):
    """
    Changes a line in a file.
    Params:
        - file_path: File path
        - line_number: Number of the line to be changed
        - new_line_content: New line content. DOES NOT INCLUDE the newline character.
    """
    # Read all lines from the file
    with open(file_path, 'r') as file:
        lines = file.readlines()
    
    # Check if the line number is valid
    if 0 <= line_number < len(lines):
        # Change the specified line
        lines[line_number] = new_line_content + '\n'
    else:
        print(f"Error: Line number {line_number} is out of range.")
        return
    
    # Write the modified lines back to the file
    with open(file_path, 'w') as file:
        file.writelines(lines)


def map_sif(sif_path:str)->None:
    """
    Maps the Elmer sif file, returing the line where the various sections start.

    """


def update_io_files(sif_path:str)->None:
    """
    Updates io file names on the Elmer sif file
    """


if __name__ == "__main__":
    results_dirpath = "../result_files/"
    postproc_dirpath = "../postproc/"
    simul_dir = ""

    cleanup_dirs(results_dirpath, postproc_dirpath)

    sun_heat_x_p = HeatBC(2, "sun_heat_x_p", 136)
    sun_heat_x_p.set_heat_flux({"value":1400, "line":136})
    sun_heat_x_p.set_heat_flux_value(1400)

    change_line_in_file(simul_dir+"case.sif", 136, "  Heat Flux = "+str(sun_heat_x_p.get_heat_flux_value()))

    os.system("ElmerSolver "+"case.sif")