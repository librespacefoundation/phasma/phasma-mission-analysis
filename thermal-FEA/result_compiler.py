import numpy as np
import pandas as pd
import vtk
from tqdm import tqdm
from vtkmodules.util import numpy_support
import os

from phasmalibma import utilities

# Parameters
abs_results_path = str(input("Please provide the absolute path to the results "
                             "directory : \n"))
timestep = int(input("Please provide the timestep between the transien "
                     "simulations : \n"))
last_sim_time = int(input("Please provide the time of the last simulation : \n"))

n_simulations = int(last_sim_time/timestep)

# Naming before model rev 7
# geometry_ids = [43, 1, 2, 31, 32, 33, 18, 5, 6, 7, 21, 34, 35]

geometry_ids = [5, 1, 2, 31, 32, 33, 43, 18, 6, 7, 8, 9, 10, 27, 28, 34, 16, 25, 17, 21, 44, 45]

# Naming after model Rev 7
# geometry_ids = [43, 1, 2, 27, 28, 29, 16, 5, 41, 42, 17]

# Create dictionaries to store the timeseries of the body temperatures
timeseries = {}
for i in range(len(geometry_ids)):
    timeseries[f"body_{geometry_ids[i]}"] = {"Times":np.zeros(n_simulations),
                                             "Minimum":np.zeros(n_simulations),
                                             "Maximum":np.zeros(n_simulations),
                                             "Mean":np.zeros(n_simulations)}

# Iterate over time and read the latest result file of each iteration
timestep_idx = 0
for time in tqdm(range(timestep, last_sim_time+timestep, timestep),
                 desc="Parsing results... "):
    dirpath = os.path.join(abs_results_path, f"post_{int(time)}")
    filename, filepath = utilities.get_last_timestep_trans_vtu(dirpath)

    # Read the .vtu file
    reader = vtk.vtkXMLUnstructuredGridReader()
    reader.SetFileName(filepath)
    reader.Update()
    unstructured_grid = reader.GetOutput()

    # Get the GeometryIds array
    geometry_ids_array = numpy_support.vtk_to_numpy(
        unstructured_grid.GetCellData().GetArray('GeometryIds'))

    # Get the temperature array
    temperature_array = numpy_support.vtk_to_numpy(
        unstructured_grid.GetPointData().GetArray('temperature'))
    # "temperature" is the name of the data array. You can see this
    # in paraview.

    for geometry_id in geometry_ids:
        body_points_ids = []

        # Find the indices of cells with the desired GeometryId
        selected_cell_indices = np.where(geometry_ids_array == geometry_id)[0]

        # Repeat for each cell of the body
        for cell_idx in range(len(selected_cell_indices)):
            cell = unstructured_grid.GetCell(selected_cell_indices[cell_idx])

            # Get the point IDs of the points of this cell
            point_ids = utilities.vtk_id_list_to_numpy(cell.GetPointIds())

            # Append the point IDs to the body points IDs list
            for p_id in point_ids:
                # Check if already in list to avoid duplicates
                if (p_id in body_points_ids):
                    continue
                else:
                    body_points_ids.append(p_id)

        # Get body temperatures
        temperatures = temperature_array[body_points_ids]
        timeseries[f"body_{geometry_id}"]["Times"][timestep_idx] = time
        timeseries[f"body_{geometry_id}"]["Maximum"][timestep_idx] = np.max(temperatures)
        timeseries[f"body_{geometry_id}"]["Minimum"][timestep_idx] = np.min(temperatures)
        timeseries[f"body_{geometry_id}"]["Mean"][timestep_idx] = np.mean(temperatures)

    timestep_idx += 1

# Save results
for i in tqdm(geometry_ids, desc="Saving ... "):
    df = pd.DataFrame(timeseries[f"body_{i}"])
    df.to_csv(f"{abs_results_path}body_{i}.csv", index=False)
