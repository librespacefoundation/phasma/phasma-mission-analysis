import argparse
import os
import shutil
import sys
from subprocess import Popen

import numpy as np
import pandas as pd

from phasmalibma import utilities, elmer

if __name__ == "__main__":

    # Configure simulation times
    external_dt = 60  # [s] This is the timestep for which the heat input files were generated and the
    # total time each transient simulation is run

    # Configure directory names and cleanup old results
    # directories are defined relatively to the simul_dirname

    # restart_dirname = "restart_files_cold_rev_8/"
    # postproc_outer_dirname = "postproc_cold_rev_8/"
    # sif_filename = "case_cold_rev_8.sif"

    parser = argparse.ArgumentParser()
    parser.add_argument("-rt", "--root", dest="simul_dirname", help="Base directory where all config files are located")
    parser.add_argument(
        "-r", "--restart_dir", dest="restart_dirname", help="Restart files directory"
    )
    parser.add_argument(
        "-p",
        "--postproc_dir",
        dest="postproc_outer_dirname",
        help="Postprocessing files directory",
    )
    parser.add_argument("-s", "--sif", dest="sif_filename", help="Elmer sif file path")

    args = parser.parse_args()

    simul_dirname = args.simul_dirname
    restart_dirname = args.restart_dirname
    postproc_outer_dirname = args.postproc_outer_dirname
    sif_filename = args.sif_filename

    os.chdir(simul_dirname)

    if not os.path.exists(restart_dirname):
        os.mkdir(restart_dirname)

    if not os.path.exists(postproc_outer_dirname):
        os.mkdir(postproc_outer_dirname)

    # utilities.cleanup_dirs(restart_dirname, postproc_outer_dirname,
                                # external_dt)

    # Configure side names and absorptivities
    sides = [
        "mx_HDRM",
        "px_HDRM",
        "mY_panel",
        "pX_panel",
        "mz_panel",
        "SIDLOC",
        "pZ_panels_no_cells",
        "pZ_panels_cells",
        "sband",
        "mY_legs",
        "pZ_legs",
        "pY_legs",
        "mZ_legs",
    ]

    clyde_cell_eff_bol = 0.307
    clyde_cell_absorptivity = 0.88
    sidloc_cell_eff_bol = 0.25

    absorptivities = [
        0.81,  # black PCB
        0.81,  # black PCB
        clyde_cell_absorptivity * (1 - clyde_cell_eff_bol),
        clyde_cell_absorptivity * (1 - clyde_cell_eff_bol),
        clyde_cell_absorptivity * (1 - clyde_cell_eff_bol),
        clyde_cell_absorptivity * (1 - sidloc_cell_eff_bol),
        0.81,  # black PCB
        clyde_cell_absorptivity * (1 - clyde_cell_eff_bol),
        0.45,  # white PCB
        0.9,
        0.9,
        0.9,
        0.9,
    ]

    areas = [
        6.35e-3,
        6.35e-3,
        2.6e-2,
        2.6e-2,
        5 * 2.6e-2,
        8.04e-3,
        2.26e-2,
        2.26e-2,
        1.3e-2,
        5.48e-6,
        5.48e-6,
        5.48e-6,
        5.48e-6,
    ]

    thermal_toolbox_absorptivity = 0.15
    thermal_toolbox_sides = ["xp", "xm", "yp", "ym", "zp", "zm"]
    thermal_toolbox_areas = [1e-2, 1e-2, 3e-2, 3e-2, 3e-2, 3e-2]

    # Read heat input files
    albedo = {}
    ir = {}
    solar = {}
    i = 0
    for side in thermal_toolbox_sides:
        cf = 1 / thermal_toolbox_areas[i] / thermal_toolbox_absorptivity
        albedo[side] = cf * np.array(
            pd.read_csv(
                "../heat_fluxes_nominal/albedo_" + side + ".csv",
                header=0,
                usecols=[1],
                dtype="float64",
            )
        )
        albedo[side] = albedo[side].reshape((albedo[side].shape[0],))
        ir[side] = cf * np.array(
            pd.read_csv(
                "../heat_fluxes_nominal/ir_" + side + ".csv",
                header=0,
                usecols=[1],
                dtype="float64",
            )
        )
        ir[side] = ir[side].reshape((ir[side].shape[0],))
        solar[side] = cf * np.array(
            pd.read_csv(
                "../heat_fluxes_nominal/solar_" + side + ".csv",
                header=0,
                usecols=[1],
                dtype="float64",
            )
        )
        solar[side] = solar[side].reshape((solar[side].shape[0],))
        i += 1

    # Configure heat input boundary conditions. Information used here are directly received from the Elmer GUI & sif file
    heat_mx_HDRM = elmer.HeatBC(sif_filename, 1, f"qin_{sides[0]}", 630, areas[0])
    heat_px_HDRM = elmer.HeatBC(sif_filename, 2, f"qin_{sides[1]}", 639, areas[1])
    heat_mY_panel = elmer.HeatBC(sif_filename, 3, f"qin_{sides[2]}", 648, areas[2])
    heat_pY_panel = elmer.HeatBC(sif_filename, 4, f"qin_{sides[3]}", 659, areas[3])
    heat_mZ_panel = elmer.HeatBC(sif_filename, 5, f"qin_{sides[4]}", 670, areas[4])
    heat_SIDLOC = elmer.HeatBC(sif_filename, 6, f"qin_{sides[5]}", 679, areas[5])
    heat_pZ_panels_no_cells = elmer.HeatBC(sif_filename, 7, f"qin_{sides[6]}", 688, areas[6])
    heat_pZ_panels_cells = elmer.HeatBC(sif_filename, 8, f"qin_{sides[7]}", 701, areas[7])
    heat_sband = elmer.HeatBC(sif_filename, 9, f"qin_{sides[8]}", 712, areas[8])
    heat_mY_legs = elmer.HeatBC(sif_filename, 10, f"qin_{sides[9]}", 721, areas[9])
    heat_pZ_legs = elmer.HeatBC(sif_filename, 11, f"qin_{sides[10]}", 730, areas[10])
    heat_pY_legs = elmer.HeatBC(sif_filename, 12, f"qin_{sides[11]}", 739, areas[11])
    heat_mZ_legs = elmer.HeatBC(sif_filename, 13, f"qin_{sides[12]}", 748, areas[12])

    # Set initial fluxes in order to configure the sif line where the heat flux inputs are located
    # note that some notepads start counting from 1
    heat_mx_HDRM.set_heat_flux({"value": 0, "sif_line": 630})
    heat_px_HDRM.set_heat_flux({"value": 0, "sif_line": 639})
    heat_mY_panel.set_heat_flux({"value": 0, "sif_line": 648})
    heat_pY_panel.set_heat_flux({"value": 0, "sif_line": 659})
    heat_mZ_panel.set_heat_flux({"value": 0, "sif_line": 670})
    heat_SIDLOC.set_heat_flux({"value": 0, "sif_line": 679})
    heat_pZ_panels_no_cells.set_heat_flux({"value": 0, "sif_line": 688})
    heat_pZ_panels_cells.set_heat_flux({"value": 0, "sif_line": 701})
    heat_sband.set_heat_flux({"value": 0, "sif_line": 712})
    heat_mY_legs.set_heat_flux({"value": 0, "sif_line": 721})
    heat_pZ_legs.set_heat_flux({"value": 0, "sif_line": 730})
    heat_pY_legs.set_heat_flux({"value": 0, "sif_line": 739})
    heat_mZ_legs.set_heat_flux({"value": 0, "sif_line": 748})

    # Configure the Elmer sif simulation section
    sif_simulation = elmer.ElmerSimulation(sif_filename, 20, 21, 23)

    # Run the transient simulations
    # start idx from 1 because the first simulation did not have any output files and was run from elmer
    n_simulations = len(solar[list(solar.keys())[0]])
    for i in range(1, n_simulations):
        # Create new result and postproc directory names
        restart_filename = restart_dirname + "out_" + str(i * external_dt) + ".result"
        output_filename = (
            restart_dirname + "out_" + str(i * external_dt + external_dt) + ".result"
        )
        postproc_inner_dirname = (
            postproc_outer_dirname + "post_" + str(i * external_dt + external_dt) + "/"
        )
        postproc_filename = postproc_inner_dirname + "case.vtu"

        # Create result and postproc directories if they do not exist.
        # Normally they should never exist because they were deleted earlier
        if not os.path.exists(postproc_inner_dirname):
            os.makedirs(postproc_inner_dirname)

        # Update result and output directory names on the sif
        sif_simulation.update_sif_io_filenames(
            restart_filename, output_filename, postproc_filename
        )

        # Update boundary condition objects and sif boundary conditions
        heat_mx_HDRM.set_heat_flux_value(
            absorptivities[0] * (solar["xm"][i] + albedo["xm"][i] + ir["xm"][i]),
            normalize_by_area=False,
        )
        heat_mx_HDRM.update_sif_heat_flux()

        heat_px_HDRM.set_heat_flux_value(
            absorptivities[1] * (solar["xp"][i] + albedo["xp"][i] + ir["xp"][i]),
            normalize_by_area=False,
        )
        heat_px_HDRM.update_sif_heat_flux()

        heat_mY_panel.set_heat_flux_value(
            absorptivities[2] * (solar["ym"][i] + albedo["ym"][i] + ir["ym"][i]),
            normalize_by_area=False,
        )
        heat_mY_panel.update_sif_heat_flux()

        heat_pY_panel.set_heat_flux_value(
            absorptivities[3] * (solar["yp"][i] + albedo["yp"][i] + ir["yp"][i]),
            normalize_by_area=False,
        )
        heat_pY_panel.update_sif_heat_flux()

        heat_mZ_panel.set_heat_flux_value(
            absorptivities[4] * (solar["zm"][i] + albedo["zm"][i] + ir["zm"][i]),
            normalize_by_area=False,
        )
        heat_mZ_panel.update_sif_heat_flux()

        heat_SIDLOC.set_heat_flux_value(
            absorptivities[5] * (solar["zp"][i] + albedo["zp"][i] + ir["zp"][i]),
            normalize_by_area=False,
        )
        heat_SIDLOC.update_sif_heat_flux()

        heat_pZ_panels_no_cells.set_heat_flux_value(
            absorptivities[6] * (solar["zp"][i] + albedo["zp"][i] + ir["zp"][i]),
            normalize_by_area=False,
        )
        heat_pZ_panels_no_cells.update_sif_heat_flux()

        heat_pZ_panels_cells.set_heat_flux_value(
            absorptivities[7] * (solar["zp"][i] + albedo["zp"][i] + ir["zp"][i]),
            normalize_by_area=False,
        )
        heat_pZ_panels_cells.update_sif_heat_flux()

        heat_sband.set_heat_flux_value(
            absorptivities[8] * (solar["zp"][i] + albedo["zp"][i] + ir["zp"][i]),
            normalize_by_area=False,
        )
        heat_sband.update_sif_heat_flux()

        heat_mY_legs.set_heat_flux_value(
            absorptivities[9] * (solar["ym"][i] + albedo["ym"][i] + ir["ym"][i]),
            normalize_by_area=False,
        )
        heat_mY_legs.update_sif_heat_flux()

        heat_pZ_legs.set_heat_flux_value(
            absorptivities[10] * (solar["zp"][i] + albedo["zp"][i] + ir["zp"][i]),
            normalize_by_area=False,
        )
        heat_pZ_legs.update_sif_heat_flux()

        heat_pY_legs.set_heat_flux_value(
            absorptivities[11] * (solar["yp"][i] + albedo["yp"][i] + ir["yp"][i]),
            normalize_by_area=False,
        )
        heat_pY_legs.update_sif_heat_flux()

        heat_mZ_legs.set_heat_flux_value(
            absorptivities[12] * (solar["zm"][i] + albedo["zm"][i] + ir["zm"][i]),
            normalize_by_area=False,
        )
        heat_mZ_legs.update_sif_heat_flux()

        # Run the updated simulation
        # subprocess.run("ElmerSolver case.sif", shell=True)
        # process = Popen("ElmerSolver case.sif", creationflags=CREATE_NEW_CONSOLE)
        print(f'Current directory is {os.getcwd()}')
        process = Popen(f"ElmerSolver {sif_filename}", shell=True)
        # process = Popen(f"ElmerSolver case_nominal_rev_8.sif")
        process.wait()
        print("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::")
        print(
            "::::::::::::::::::: COMPLETED Sim #%d / %d :::::::::::::::::::"
            % (i, n_simulations)
        )
        print("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::")
        # time.sleep(10)
