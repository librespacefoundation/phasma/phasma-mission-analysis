import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


def find_lims(
    times: np.ndarray, temperatures: np.ndarray, orbit_period: float, timestep: float
):
    """
    Heuristic function to determine the range of temperatures a component
    experiences. Relies on the assumption that the solution of the problem
    has converged the last 2 orbits of the available data and thus uses the
    data points over those to determine the range.
    It is suggested to visually verify the results as well.
    """
    orbit_points = orbit_period / timestep
    converged_points = int(2 * orbit_points)
    T_max = np.max(temperatures[-converged_points:-1])
    T_min = np.min(temperatures[-converged_points:-1])
    return [T_min, T_max]


# results_path = str(input("Please provide the path to the results directory : \n"))
# timestep = int(input("Please provide the timestep between the transient simulations : \n"))
# last_sim_start_time = int(input("Please provide the time at which the last simulation started : \n"))
# body = str(input("Please provide the body of interest (only works for 'panel_1') : \n"))

results_path = "full_model/Rev_6/postproc_cold_rev_8/"

body_ids = [5, 1, 2, 31, 32, 33, 43, 18, 6, 7, 8, 9, 10, 27, 28, 34, 16, 25, 17, 21]

for body_id in body_ids:
    df = pd.read_csv(f"{results_path}body_{body_id}.csv", header=0, dtype="float64")
    times = np.array(df["Times"])
    T_mean = np.array(df["Mean"])
    limit_temps = find_lims(times, T_mean - 273, 5600, 60)

    fig, ax = plt.subplots()
    ax.plot(times, T_mean - 273)
    ax.hlines(-40, 0, np.max(times))
    ax.set_title(f"body {body_id} temperature evolution")
    ax.set_ylabel("Temperature [C]")
    ax.set_xlabel("Time [s]")
    ax.grid(linestyle="--", alpha=0.7)

    print(f"Body {body_id} min temp = {np.round(limit_temps[0], 1)}")
    print(f"Body {body_id} max temp = {np.round(limit_temps[1], 1)}")
    print("------")
    plt.show()
