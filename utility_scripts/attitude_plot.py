import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import argparse
import phasmalibma.utilities
import phasmalibma.plots


def generate_plots(simul_dir, save_dir=None):
    # read the timestep from the Inp_sim file
    inp_sim_path = simul_dir + "Inp_Sim.txt"
    timestep = phasmalibma.utilities.find_42_timestep(inp_sim_path)

    # ================== Plot Euler angles in the L frame =====================
    # The L frame is the Local-Vertical Local-Horizontal (LVLH) frame
    eulang_path = simul_dir + "RPY.42"

    if save_dir is not None:
        savepath = save_dir + "RPY.png"
    else:
        savepath = None

    phasmalibma.plots.plot_euler_angles(eulang_path, timestep, savepath=savepath)

    # ==================== Plot Quaternions in the N frame ====================
    quat_path = simul_dir + "qbn.42"

    if save_dir is not None:
        savepath = save_dir + "qbn.png"
    else:
        savepath = None

    phasmalibma.plots.plot_qbn(quat_path, timestep, savepath=savepath)

    # ================== Plot angular velocity in the N frame =================
    # The L frame is the Local-Horizontal Local-Vertical (LHLV) frame
    omega_path = simul_dir + "wbn.42"

    if save_dir is not None:
        plt.savefig(save_dir + "wbn.png", bbox_inches="tight")
        savepath = save_dir + "wbn.png"
    else:
        savepath = None

    phasmalibma.plots.plot_wbn(omega_path, timestep, savepath=savepath)

    # ==================== Plot Quaternions in the L frame ====================
    quat_path = simul_dir + "qbl.42"

    if save_dir is not None:
        plt.savefig(save_dir + "qbl.png", bbox_inches="tight")
        savepath = save_dir + "qbl.png"
    else:
        savepath = None
    
    phasmalibma.plots.plot_qbl(quat_path, timestep, savepath=savepath)

    # ================== Plot angular velocity in the N frame =================
    # The L frame is the Local-Horizontal Local-Vertical (LHLV) frame
    omega_path = simul_dir + "wbl.42"

    if save_dir is not None:
        plt.savefig(save_dir + "wbl.png", bbox_inches="tight")
        savepath = save_dir + "wbl.png"
    else:
        savepath = None
    
    phasmalibma.plots.plot_wbl(omega_path, timestep, savepath=savepath)

    plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-s", "--simdir", dest="simul_dir", help="Attitude simulation results directory"
    )
    parser.add_argument(
        "-p",
        "--plotdir",
        dest="save_dir",
        help="Directory to store attitude plots",
        default=None,
    )

    args = parser.parse_args()

    print("Loading parameters from", args.simul_dir)
    print("Saving result plots to ", args.save_dir)

    generate_plots(simul_dir=args.simul_dir, save_dir=args.save_dir)
