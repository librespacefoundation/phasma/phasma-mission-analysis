import matplotlib.pyplot as plt
import argparse

import phasmalibma.plots
import phasmalibma.utilities

parser = argparse.ArgumentParser()
parser.add_argument(
    "-s", "--simdir", dest="simul_dir", help="Attitude simulation results directory"
)
parser.add_argument(
    "-p",
    "--plotdir",
    dest="save_dir",
    help="Directory to store attitude plots",
    default=None,
)

args = parser.parse_args()

print("Loading parameters from", args.simul_dir)
print("Saving result plots to ", args.save_dir)

save_dir = args.save_dir
simul_dir = args.simul_dir

# read the timestep from the Inp_sim file
inp_sim_path = simul_dir + "Inp_Sim.txt"

timestep = phasmalibma.utilities.find_42_timestep(inp_sim_path)

# ================== Plot reaction wheel momentum =======================
rw_path = simul_dir + "Hwhl.42"

if save_dir is not None:
    savepath = save_dir + "Hwhl.png"
else:
    savepath = None

phasmalibma.plots.plot_rw_mom(rw_path, n_rw=3, timestep=timestep, savepath=savepath)

# ================= Plot magnetorquer magnetic dipole moment =========
mtb_path = simul_dir + "MTB.42"

if save_dir is not None:
    savepath = save_dir + "MTB.png"
else:
    savepath = None

phasmalibma.plots.plot_mtq_mdm(mtb_path, n_mtq=3, timestep=timestep, savepath=savepath)

plt.show()
