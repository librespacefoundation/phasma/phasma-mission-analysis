# Adding new controllers to 42
A detailed process on how to add new controllers to 42 is described in [1]. For the various simulations included in this repository, one can either use the included files in the `simulation_dir/42_files`, ("simulation_dir" is the name of the specific simulation case (e.g. unactuated, rw_3ax_desat)), or make the changes in their local installation of 42 as described in [1].

[1] [Study of orbital propagator 42 for missions based on constellations of nanosatellites](https://upcommons.upc.edu/bitstream/handle/2117/361387/TFM.pdf?sequence=3).