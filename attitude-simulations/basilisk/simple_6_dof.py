import os
from copy import copy

import matplotlib.pyplot as plt
import numpy as np

from Basilisk import __path__

bskPath = __path__[0]
fileName = os.path.basename(os.path.splitext(__file__)[0])


# import simulation related support
from Basilisk.simulation import spacecraft
# general support file with common unit test functions
# import general simulation support files
from Basilisk.utilities import (SimulationBaseClass, macros, orbitalMotion,
                                simIncludeGravBody, unitTestSupport, vizSupport)


def run(sim_timestep:float, sim_time:float):

    # Convert simulation time and timestep to nanoseconds
    sim_time = macros.sec2nano(sim_time)
    sim_timestep = macros.sec2nano(sim_timestep)

    # Create simulation variable names
    dynamics_task_name = "Dynamics Task"
    main_process_name = "Main Process"

    # Create a sim module as an empty container
    sim = SimulationBaseClass.SimBaseClass()

    # Create the simulation process
    main_process = sim.CreateNewProcess(main_process_name)

    # Create the dynamics task and specify the integration update time
    main_process.addTask(sim.CreateNewTask(dynamics_task_name, sim_timestep))

    # Create a spacecraft object
    sc_object = spacecraft.Spacecraft()
    sc_object.ModelTag = "Phasma SC module"
    # Define the simulation inertia
    moi = [9.0, 0., 0.,
           0., 8.0, 0.,
           0., 0., 6.0]
    sc_object.hub.mHub = 4.5  # kg - spacecraft mass
    sc_object.hub.r_BcB_B = [[0.0], [0.0], [0.0]]  # m - position vector of body-fixed point B relative to CM
    sc_object.hub.IHubPntBc_B = unitTestSupport.np2EigenMatrix3d(moi)

    # Add the spacecraft object to the simulation process
    sim.AddModelToTask(dynamics_task_name, sc_object)

    # Set up gravitational forces
    # see the scenarioOrbitMultiBody.py to simulate multiple bodies using SPICE
    gravFactory = simIncludeGravBody.gravBodyFactory()
    planet = gravFactory.createEarth()
    planet.isCentralBody = True  # ensure this is the central grav. body
    planet.useSphericalHarmonicsGravityModel(bskPath + '/supportData/LocalGravData/GGM03S-J2-only.txt', 2)  # NOQA: E501
    mu = planet.mu
    # Connect gravitational bodies to the spacecraft object
    gravFactory.addBodiesTo(sc_object)

    # Set up the orbit using orbital elements
    oe = orbitalMotion.ClassicElements()
    rLEO = 7000. * 1000      # meters
    oe.a = rLEO
    oe.e = 0.0001
    oe.i = 33.3 * macros.D2R
    oe.Omega = 48.2 * macros.D2R
    oe.omega = 347.8 * macros.D2R
    oe.f = 85.3 * macros.D2R
    rN, vN = orbitalMotion.elem2rv(mu, oe)

    # Convert back to oe since some changes might have occured during
    # the oe-to-rv conversion (this is done in the basilisk-provided)
    # example, which mentions that "with circular or equatorial orbit,
    # some angles are arbitrary"
    oe = orbitalMotion.rv2elem(mu, rN, vN)

    # Set the spacecraft initial r-v
    sc_object.hub.r_CN_NInit = rN  # m   - r_BN_N
    sc_object.hub.v_CN_NInit = vN  # m/s - v_BN_N

    # Set the initial spacecraft attitude
    sc_object.hub.sigma_BNInit = [[0.0], [0.0], [-0.3]]  # sigma_BN_B
    sc_object.hub.omega_BN_BInit = [[0.001], [-0.01], [0.03]]  # rad/s - omega_BN_B

    # set the simulation time
    n = np.sqrt(mu / oe.a / oe.a / oe.a)
    P = 2. * np.pi / n

    # create a logging task object of the spacecraft output message at the desired down sampling ratio
    data_rec = sc_object.scStateOutMsg.recorder()
    sim.AddModelToTask(dynamics_task_name, data_rec)

    # Vizualize simulation in Vizard
    # viz = vizSupport.enableUnityVisualization(sim, dynamics_task_name,
    #                                           sc_object,
    #                                           # saveFile=__file__
    #                                           # liveStream=True
    #                                           )

    # Initialize simulation
    sim.InitializeSimulation()

    # Show a simulation progress bar in the terminal
    sim.SetProgressBar(True)

    # Configure a simulation stop time and execute the simulation run
    sim.ConfigureStopTime(sim_time)
    sim.ExecuteSimulation()

    # Retrieve the logged data
    pos_data = data_rec.r_BN_N
    vel_data = data_rec.v_BN_N
    attitude_data = data_rec.sigma_BN
    ang_vel_data = data_rec.omega_BN_B

    return pos_data, vel_data, attitude_data, ang_vel_data


if __name__ == "__main__":
    sim_timestep = 1
    sim_time = 11600
    pos_data, vel_data, attitude_data, ang_vel_data = run(sim_timestep, sim_time)

    time = np.arange(0, sim_time+sim_timestep, sim_timestep)
    fig, ax = plt.subplots()
    l0, = ax.plot(time, ang_vel_data[:, 0], label="sigma 1")
    l1, = ax.plot(time, ang_vel_data[:, 1], label="sigma 2")
    l2, = ax.plot(time, ang_vel_data[:, 2], label="sigma 3")
    ax.legend(handles=[l0, l1, l2])

    plt.show()
