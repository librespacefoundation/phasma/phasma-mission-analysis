import argparse


def rescale_obj(obj_path, obj_scaled_path, scale):
    with open(obj_path, "r") as source:
        with open(obj_scaled_path, "w") as target:
            for line in source:
                taget_line = line
                prefix = ""
                if line.startswith(("v ", "vt", "vn")):
                    splitter = " "  # default splitter for vt and vn
                    prefix = line.split(splitter)[0]
                    coordinates = [
                        float(coordinate) for coordinate in line.split(splitter)[1:]
                    ]
                    rescaled = [c * scale for c in coordinates]
                    rescaled_as_str = " ".join([str(c) for c in rescaled])
                    taget_line = f"{prefix} {rescaled_as_str}\n"

                target.write(taget_line)


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--orig", dest="orig_path", help="Original obj file")
    parser.add_argument(
        "-f",
        "--final",
        dest="final_path",
        help="Scaled obj file",
        default="scaled_model.obj",
    )
    parser.add_argument(
        "-s",
        "--scale",
        dest="scale",
        help="Scaling factor",
        default=0.001,
    )

    args = parser.parse_args()

    # obj_path = "3d-models/Phasma_stowed_orig.obj"
    # obj_scaled_path = "3d-models/Phasma_stowed.obj"
    # scale = 0.01

    rescale_obj(args.orig_path, args.final_path, float(args.scale))
