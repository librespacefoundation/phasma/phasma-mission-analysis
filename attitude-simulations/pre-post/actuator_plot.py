import matplotlib.pyplot as plt 
import pandas as pd
import numpy as np

# simulation input-output file directory
simul_dir = "ADCS/rw_3ax_desat/"

# read the timestep from the Inp_sim file
inp_sim_path = simul_dir + "Inp_Sim.txt"
with open(inp_sim_path,'r') as input_file: 
    linenum = 0     
    # File output interval information is located in line 5
    for line in input_file : 
        if linenum < 4 : 
            pass
        elif linenum == 4 : 
            timestep = line.split()
            timestep = float(timestep[0])
        else :
            break
        linenum += 1 

# Define colors for plotting 
colors = ["dodgerblue","forestgreen","firebrick",'darkmagenta']

# ================== Plot reaction wheel momentum =======================
# Written for 3 wheels, easily extensible for more 

rw_path = simul_dir + "Hwhl.42"
colnames = ['Wheel 0','Wheel 1','Wheel 2']
Hwhl = pd.read_csv(rw_path,delimiter = ' ',header=None,usecols = [0,1,2],names = colnames) # usecols is needed because 42 adds a white space character at the end and python reads an extra collumn

# construct time array
datasize = Hwhl.shape[0]
times = np.zeros(datasize)
for i in range(1,datasize):
    times[i] = times[i-1] + timestep

# Define max and min plot time in case a window is studied
min_plot_time = 0 
max_plot_time = max(times)


fig1,ax1 = plt.subplots()
print(Hwhl)
l1, = ax1.plot(times,Hwhl['Wheel 0'], label = "Wheel 0",c= colors[0])
l2, = ax1.plot(times,Hwhl['Wheel 1'], label = "Wheel 1",c= colors[1])
l3, = ax1.plot(times,Hwhl['Wheel 2'], label = "Wheel 2",c= colors[2])
handles = [l1,l2,l3]

ax1.grid()
ax1.set_title("Reaction wheel angular momentum")
ax1.set_ylabel("Angular momentum [Nms]")
ax1.set_xlabel("Time [sec]")
ax1.legend(handles = handles)
ax1.set_xlim([min_plot_time,max_plot_time])


# ==================== Plot magnetorquer magnetic dipole moment ====================
# Written for 3 magnetorquers - easily extensible for more

mtb_path = simul_dir + "MTB.42"
colnames = ['MTB 0','MTB 1','MTB 2']
mtb = pd.read_csv(mtb_path,delimiter = ' ',header=None,usecols = [0,1,2],names = colnames) # usecols is needed because 42 adds a white space character at the end and python reads an extra collumn

fig2,ax2 = plt.subplots()
l1, = ax2.plot(times,mtb['MTB 0'], label = "MTB 0",c= colors[0])
l2, = ax2.plot(times,mtb['MTB 1'], label = "MTB 1",c= colors[1])
l3, = ax2.plot(times,mtb['MTB 2'], label = "MTB 2",c= colors[2])
handles = [l1,l2,l3]

ax2.grid()
ax2.set_title("Magnetorquer magnetic dipole moment")
ax2.set_ylabel("Magnetic dipole moment Am^2")
ax2.set_xlabel("Time [sec]")
ax2.legend(handles = handles)
ax2.set_xlim([min_plot_time,max_plot_time])

plt.show()