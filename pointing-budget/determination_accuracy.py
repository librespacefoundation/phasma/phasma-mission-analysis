import numpy as np
import pandas as pd
from scipy.spatial.transform import Rotation
import matplotlib.pyplot as plt

import phasmalibma.utilities

simul_dir = "pointing-budget/determination/"

# read the timestep from the Inp_sim file
timestep = phasmalibma.utilities.find_42_timestep(simul_dir)

# Define colors for plotting
colors = ["dodgerblue", "forestgreen", "firebrick", "darkmagenta"]

# Get attitude estimation results from 42
estimates_path = simul_dir + "KF.42"
estimates_colnames = ["qbn_0", "qbn_1", "qbn_2", "qbn_3", "wbn_0", "wbn_1", "wbn_2"]
estimates_df = pd.read_csv(
    estimates_path,
    delimiter=" ",
    header=None,
    names=estimates_colnames,
    usecols=[0, 1, 2, 3, 4, 5, 6],
    dtype="float64",
)
estimates = np.array(estimates_df)

# Get real attitude results from 42
actual_att_path = simul_dir + "qbn.42"
actual_att_colnames = ["q1", "q2", "q3", "q0"]
actual_att_df = pd.read_csv(
    actual_att_path, delimiter=" ", header=None, names=actual_att_colnames
)
actual_att = np.array(actual_att_df)

# Create a time array
time = timestep * np.arange(0, actual_att.shape[0], 1)

# Get the relative quaternion between the actual and the estimated (direction does not matter)
# Convert quaternions to rotation objects
rot_est = Rotation.from_quat(estimates[:, 0:4])
rot_actual = Rotation.from_quat(actual_att)
rot_rel = rot_actual * rot_est.inv()
rot_vectors = rot_rel.as_rotvec(degrees=True)
error_angles = np.linalg.norm(rot_vectors, axis=1)

# Calculate error distribution standard deviation
# Crop the error values array because it might contain excessive error values (e.g. during eclipse)
stat_time_start = 2000
stat_idx_start = int(stat_time_start / timestep)
stat_time_end = 2300
stat_idx_end = int(stat_time_end / timestep)
error_angles_std = np.std(error_angles[stat_idx_start:stat_idx_end])
error_angles_mean = np.mean(error_angles[stat_idx_start:stat_idx_end])
print("Attitude determination error sigma = ", error_angles_std, " [deg]")
print("Attitude determination error mean = ", error_angles_mean, " [deg]")

# Plot error
fix, ax = plt.subplots()
ax.plot(time, error_angles)
ax.set_xlabel("Time [s]")
ax.set_ylabel("Determination error [deg]")
ax.set_title("Attitude determination error in sunlight")
ax.grid(linestyle="--", alpha=0.7)

plt.show()
