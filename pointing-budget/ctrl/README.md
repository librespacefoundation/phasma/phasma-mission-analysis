# Attitude control error simulation
A simulation to determine the achieved attitude control error under perfect state knowledge. This simulation quantifies the error from the actuators and controller disturbances from the environment, but perfect full state knowledge.

To perform correct solar radiation pressure and aerodynamic torque calculations, one must use the 3D model of their satellite. Said 3D model must be in `.obj` form and it must be included in the `Model/` directory of 42. The .obj used for the present simulation is included in the repository.  
Moreover, the TLE or orbit data as well as the launch date (found in the `Orb_PASMA.txt`-`TLE.txt` and `Inp_Sim.txt` files respectively) must be tuned before running the simulation.

**Note 1 :** The coordinates of the center of mass are related to the coordinate system of the CAD model. Keep this in mind in case the CM coordinates in the configuration files seem odd and make sure to verify with the aid of the graphical interface.

**Note 2 :** (making the note here because 42 does not allow notes in config files) The simulation configuration files used in the configuration files refer to the stowed configuration.