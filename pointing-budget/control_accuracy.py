import numpy as np
import pandas as pd
from scipy.spatial.transform import Rotation
import matplotlib.pyplot as plt

from mpl_toolkits.axes_grid1.inset_locator import mark_inset

import phasmalibma.utilities

simul_dir = "pointing-budget/control/"

# read the timestep from the Inp_sim file
timestep = phasmalibma.utilities.find_42_timestep(simul_dir)

# Define colors for plotting
colors = ["dodgerblue", "forestgreen", "firebrick", "darkmagenta"]

# Get actual attitude results from 42
actual_att_path = simul_dir + "qbl.42"
actual_att_colnames = ["q1", "q2", "q3", "q0"]
actual_att_df = pd.read_csv(
    actual_att_path, delimiter=" ", header=None, names=actual_att_colnames
)
actual_att = np.array(actual_att_df)

# Construct commanded attitude array
cmd = np.array([0.0, 0.0, 0.0, 1.0])  # nadir pointing is always commanded
des_att = np.ones(actual_att.shape)
des_att = des_att * cmd

# Create a time array
time = timestep * np.arange(0, actual_att.shape[0], 1)

# Get the relative quaternion between the actual and the estimated (direction does not matter)
# Convert quaternions to rotation objects
rot_des = Rotation.from_quat(des_att)
rot_actual = Rotation.from_quat(actual_att)
rot_rel = rot_actual * rot_des.inv()
rot_vectors = rot_rel.as_rotvec(degrees=True)
error_angles = np.linalg.norm(rot_vectors, axis=1)

# Calculate control error standard deviation
# Crop the error values array because it might contain excessive error values (e.g. transient condition)
stat_time_start = 2000
stat_idx_start = int(stat_time_start / timestep)
stat_time_end = 2300
stat_idx_end = int(stat_time_end / timestep)
error_angles_std = np.std(error_angles[stat_idx_start:stat_idx_end])
error_angles_mean = np.mean(error_angles[stat_idx_start:stat_idx_end])
print("Attitude control error sigma = ", error_angles_std, " [deg]")
print("Attitude control error mean  = ", error_angles_mean, " [deg]")

# Plot control error
fig, ax = plt.subplots()
ax.plot(time, error_angles)
ax.set_xlabel("Time [s]")
ax.set_ylabel("Control error [deg]")
ax.set_title("Attitude control error with perfect state knowledge")
ax.grid(linestyle="--", alpha=0.7)

# Add an inset to the main plot
x1_zoom, x2_zoom, y1_zoom, y2_zoom = 200, 400, -0.1, 0.1

ax_inset = fig.add_axes(
    [0.4, 0.4, 0.4, 0.4]
)  # Control the position and size of the inset [left, bottom, width, height]
ax_inset.plot(time, error_angles)
ax_inset.set_xlim(x1_zoom, x2_zoom)
ax_inset.set_ylim(y1_zoom, y2_zoom)
ax_inset.grid(linestyle="--", alpha=0.7)
# ax_inset.set_title('Zoomed Region')

# draw a bbox of the region of the inset axes in the parent axes and
# connecting lines between the bbox and the inset axes area
mark_inset(ax, ax_inset, loc1=4, loc2=3, fc="none", ec="0.5")

plt.show()
