import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

import phasmalibma.utilities

simul_dir = "pointing-budget/determination/"


# read the timestep from the Inp_sim file
timestep = phasmalibma.utilities.find_42_timestep(simul_dir)

# Define colors for plotting
colors = ["dodgerblue", "forestgreen", "firebrick", "darkmagenta"]

# ================== Plot KF predictions  =======================
KF_path = simul_dir + "KF.42"
colnames = ["qbl_0", "qbl_1", "qbl_2", "qbl_3", "wbl_0", "wbl_1", "wbl_2"]
KF = pd.read_csv(
    KF_path,
    delimiter=" ",
    header=None,
    names=colnames,
    usecols=[0, 1, 2, 3, 4, 5, 6],
    dtype="float64",
)

# construct time array
datasize = KF.shape[0]
times = np.zeros(datasize)
for i in range(1, datasize):
    times[i] = times[i - 1] + timestep

fig, (ax1, ax2) = plt.subplots(2, 1)
(l1,) = ax1.plot(times, KF["qbl_0"], label="qbl_0", c=colors[0])
(l2,) = ax1.plot(times, KF["qbl_1"], label="qbl_1", c=colors[1])
(l3,) = ax1.plot(times, KF["qbl_2"], label="qbl_2", c=colors[2])
(l4,) = ax1.plot(times, KF["qbl_3"], label="qbl_3", c=colors[3])
handles = [l1, l2, l3, l4]

ax1.grid()
ax1.set_title("Predicted qbl")
ax1.set_ylabel("Quaternion")
ax1.set_xlabel("Time [sec]")
ax1.legend(handles=handles)

(l1,) = ax2.plot(times, KF["wbl_0"], label="wbl_0", c=colors[0])
(l2,) = ax2.plot(times, KF["wbl_1"], label="wbl_1", c=colors[1])
(l3,) = ax2.plot(times, KF["wbl_2"], label="wbl_2", c=colors[2])
handles = [l1, l2, l3]

ax2.grid()
ax2.set_title("Predicted wbl")
ax2.set_ylabel("Angular velocity (rad/sec)")
ax2.set_xlabel("Time [sec]")
ax2.legend(handles=handles)

print(KF["qbl_3"])

plt.tight_layout()
plt.show()
