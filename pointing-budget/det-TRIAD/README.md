# Attitude determination error simulation
A simulation to determine the achieved attitude determination error assuming an uncontrolled system. The determination error is calculated for the sunlit part of the orbit using the TRIAD algorithm for attitude determination. The sensors used are a 3-axis magnetometer and a fine sun sensor.

For the simulation to export attitude estimation data, a modified version of the 42 simulator is used, which can be found in [this fork](git@gitlab.com:dimoustroufis/42.git).

To perform correct solar radiation pressure and aerodynamic torque calculations, one must use the 3D model of their satellite. Said 3D model must be in `.obj` form and it must be included in the `Model/` directory of 42. The .obj used for the present simulation is included in the repository.  
Moreover, the TLE or orbit data as well as the launch date (found in the `Orb_PASMA.txt`-`TLE.txt` and `Inp_Sim.txt` files respectively) must be tuned before running the simulation.

**Note 1 :** The coordinates of the center of mass are related to the coordinate system of the CAD model. Keep this in mind in case the CM coordinates in the configuration files seem odd and make sure to verify with the aid of the graphical interface.

**Note 2 :** (making the note here because 42 does not allow notes in config files) The simulation configuration files used in the configuration files refer to the stowed configuration.