import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# simulation input-output file directory
simul_dir = "config_42_onr/"

# read the timestep from the Inp_sim file
inp_sim_path = simul_dir + "Inp_Sim.txt"
with open(inp_sim_path, "r") as input_file:
    linenum = 0
    # File output interval information is located in line 5
    for line in input_file:
        if linenum < 4:
            pass
        elif linenum == 4:
            timestep = line.split()
            timestep = float(timestep[0])
        else:
            break
        linenum += 1

# Define colors for plotting
colors = ["dodgerblue", "forestgreen", "firebrick", "darkmagenta"]


# Read actuator mangetic dipole moment results from 42
mtb_path = simul_dir + "MTB.42"
mtb_colnames = ["Wheel 0", "Wheel 1", "Wheel 2"]
mtb = pd.read_csv(
    mtb_path, delimiter=" ", header=None, usecols=[0, 1, 2], names=mtb_colnames
)  # usecols is needed because 42 adds a white space character at the end and python reads an extra collumn
mtb = np.array(mtb)


# construct time array
times = np.zeros(mtb.shape[0])
for i in range(1, mtb.shape[0]):
    times[i] = times[i - 1] + timestep

# Set a cuttoff time. Useful for cases when the simulation has been run
# for longer than required, but the detumbling gain is such that even for
# small angular velocities the actuators are driven to a high output resuling
# in increased power draw. In a real case, the ADCS would detect that angular
# rates have decreased an exit the detumbling mode.
# cutoff_time = np.max(times)  # default is no cutoff
cutoff_time = 5000
cutoff_idx = int(cutoff_time / timestep)  # +- 1 row does not make a difference
times = times[:cutoff_idx]  # cut times
mtb = mtb[:cutoff_idx, :]  # cut mtb

# Calculate MTB power for every timepoint
# If m is the magnetic dipole mmoment (output of 42), gm is the magnetic gain [Am^2/A],
# and R the coil resistance [Ohm], then the power is calculated as: P = (m/gm)^2*R [W]

gm = [2.3, 4.3, 4.3]  # 2.3 Am^2/A for the CROOO2 and 4.3 for the CR0003
R = [51.0, 66.5, 66.5]  # 51 Ohm for the CR0002 and 66.5 Ohm for the CR0003

mtb_power = np.zeros(mtb.shape)

for i in range(mtb.shape[1]):
    mtb_power[:, i] = mtb[:, i] ** 2 / gm[i] ** 2 * R[i]

# Integrate to get total energy required [Ws] and convert to [Wh]
mtb_energy = np.zeros(mtb_power.shape[1])
ws_to_wh = 1 / 3600
for i in range(mtb_power.shape[1]):
    mtb_energy[i] = np.trapz(mtb_power[:, i], times) * ws_to_wh

# Plot magnetorquer power and print magnetorquer energy
fig, ax = plt.subplots()
(l0,) = ax.plot(times, mtb_power[:, 0], c=colors[0], label="MTB 0 power")
(l1,) = ax.plot(times, mtb_power[:, 1], c=colors[1], label="MTB 0 power")
(l2,) = ax.plot(times, mtb_power[:, 2], c=colors[2], label="MTB 0 power")
ax.set_ylabel("Power [W]")
ax.set_xlabel("Time [s]")
ax.set_title("Magnetorquer power")
ax.grid(linestyle="--", alpha=0.7)

plt.show()

print(
    "Energy requirements: MTB[0]=%lf || MTB[1]=%lf || MTB[2]=%lf [Wh]"
    % (mtb_energy[0], mtb_energy[1], mtb_energy[2])
)
