# Detumbling analysis
This directory contains simulation configuration files and postprocessing functions that are used to analyze the detumbling phase of the mission. More specifically, the following questions are sought to be answered:
1. How long is detumbling projected to last ?
2. Is the EPS capable of providing power throughout the detumbling phase without entering a "battery critical" mode and shutting the ADCS down ?

## Structure
This directory is structured as follows:
```
detumbling-analysis/
├─ config_42_nr: Contains 42 simulator configuration files to simulate detumbling from nominal tip-off rates.
├─ config_42_onr: Contains 42 simulator configuration files to simulate detumbling from off-nominal tip-off rates.
├─ 42_files: Contains the detumbling function to be used in the 42 simulator
```
## Usage
After integrating the provided flight software function into 42, one can tune the detumbling gain in the 42 source code, recompile, and run.

## Remarks
To perform correct solar radiation pressure and aerodynamic torque calculations, one must use the 3D model of their satellite. Said 3D model must be in `.obj` form and it must be included in the `Model/` directory of 42. The .obj used for the present simulation is included in the repository. For the detumbling analysis use the `3d-models/Phasma_stowed.obj` model.
Moreover, the TLE or orbit data as well as the launch date (found in the `Orb_PHASMA.txt`-`TLE.txt` and `Inp_Sim.txt` files respectively) must be tuned before running the simulation.

**Note 1 :** The coordinates of the center of mass are related to the coordinate system of the CAD model. Keep this in mind in case the CM coordinates in the configuration files seem odd and make sure to verify with the aid of the graphical interface.

**Note 2 :** (making the note here because 42 does not allow notes in config files) The simulation configuration files used in the configuration files refer to the stowed configuration.

**Note 3 :** If using the off-time feature of the detumbling controller make sure that the file output timestep is not set as such that it coincides with the times when magnetorquer dipole moment is set to 0.0.
