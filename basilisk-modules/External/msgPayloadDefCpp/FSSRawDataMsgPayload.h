#ifndef FSSRAWDATA_MSG
#define FSSRAWDATA_MSG

/**
 * @brief Message containing the output of a Fine Sun Sensor
 * 
 */
typedef struct{
    double sHat_B[3]; /* Unit vector of the sun in the spacecraft body frame */
    bool isValid; /* Flag indicating whether the sensor output is valid or not */

}FSSRawDataMsgPayload;

#endif
