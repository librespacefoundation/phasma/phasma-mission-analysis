// Since some functions were heavily influenced from the CoarseSunSensor provided by Basilisk,
// it was deemed appropriate to include its copyright notice in this code, as its licence requires.
/*
 ISC License

 Copyright (c) 2016, Autonomous Vehicle Systems Lab, University of Colorado at Boulder

 Permission to use, copy, modify, and/or distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.

 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

 */

#include "fineSunSensor.h"
#include <math.h>
#include <iostream>
#include "architecture/utilities/avsEigenSupport.h"
#include "architecture/utilities/macroDefinitions.h"
#include "architecture/utilities/avsEigenMRP.h"
#include "architecture/utilities/rigidBodyKinematics.h"


FineSunSensor::FineSunSensor(){
    this->noise1Sigma = 0.0;
    this->fov = M_PI/2;
    this->dcm_BFSS = Eigen::Matrix3d::Identity();
    this->nHat_FSS << 0.0, 0.0, -1.0;
    this->nHat_B << 0.0, 0.0, -1.0;

    // Assign zero payload to the input messages
    this->stateCurrent = this->stateInMsg.zeroMsgPayload;
    this->sunVisibilityFactor = this->eclipseInMsg.zeroMsgPayload;
    this->sunData = this->sunInMsg.zeroMsgPayload;

    std::random_device rd;
    this->random_engine = std::mt19937(rd());
    this->distribution = std::normal_distribution<double>(0.0, this->noise1Sigma);
}


FineSunSensor::~FineSunSensor(){

}


void FineSunSensor::Reset(uint64_t current_sim_nanos){
    if(!this->sunInMsg.isLinked()){
        bskLogger.bskLog(BSK_ERROR, "FineSunSensor: Failed to link a sun sensor input message");
    }

    if(!this->stateInMsg.isLinked()){
        bskLogger.bskLog(BSK_ERROR, "FineSunSensor: Failed to link a spacecraft state input message");
    }

    if(!this->eclipseInMsg.isLinked()){
        bskLogger.bskLog(BSK_ERROR, "FineSunSensor: Failed to link an eclipse input message");
    }

    // Compute sensor boresight heading vector in the body frame
    this->nHat_B = this->dcm_BFSS * this->nHat_FSS;
}


/**
 * @brief Reads input messages and populates the respective class members
 * 
 */
void FineSunSensor::readInputMessages(){
    // Zero input messages' content
    this->sunData = this->sunInMsg.zeroMsgPayload;
    this->stateCurrent = this->stateInMsg.zeroMsgPayload;
    this->sunVisibilityFactor = this->eclipseInMsg.zeroMsgPayload;

    this->sunData = this->sunInMsg();
    this->stateCurrent = this->stateInMsg();
    this->sunVisibilityFactor = this->eclipseInMsg();
}


void FineSunSensor::writeOutputMessage(uint64_t current_sim_nanos){
    if(this->fssDataOutMsg.isLinked()){
        FSSRawDataMsgPayload dataMsg;
        dataMsg = this->fssDataOutMsg.zeroMsgPayload;
        dataMsg.sHat_B[0] = this->sHat_B[0];
        dataMsg.sHat_B[1] = this->sHat_B[1];
        dataMsg.sHat_B[2] = this->sHat_B[2];
        dataMsg.isValid = this->isValid;
        this->fssDataOutMsg.write(&dataMsg, this->moduleID, current_sim_nanos);
    }
}


/**
 * @brief Calculates the sun heading unit vector in the body frame
 * 
 * Heavily influenced by the counterpart function of the CoarseSunSensor module
 */
void FineSunSensor::findSunVector(){
    Eigen::Vector3d r_sat_sun_N; // vector from the satellite to the sun, in the inertial frame
    Eigen::Vector3d sHat_N; // sun vector in the inertial frame
    Eigen::Matrix3d dcm_BN; // Rotation matrix from the inertial frame to the body frame

    // Store some variables in Eigen types to facilitate mathematical operations
    Eigen::Vector3d r_BN_N;
    Eigen::Vector3d r_sun_N;
    Eigen::MRPd sigma_BN_eigen;

    // Convert input message data to Eigen types
    r_BN_N = cArray2EigenVector3d(this->stateCurrent.r_BN_N);
    r_sun_N = cArray2EigenVector3d(this->sunData.PositionVector);
    sigma_BN_eigen = cArray2EigenMRPd(this->stateCurrent.sigma_BN);

    // Determine sun heading vector in the inertial frame
    r_sat_sun_N = r_sun_N - r_BN_N;
    sHat_N = r_sat_sun_N / r_sat_sun_N.norm();

    // Determin sun heading vector in the body frame
    dcm_BN = sigma_BN_eigen.toRotationMatrix().transpose();
    this->sHat_B = dcm_BN * sHat_N;
}


/**
 * @brief Limits the sensor's output based on eclipse and FOV considerations
 * 
 */
void FineSunSensor::limitOutput(){
    // Determine whether the satellite is in eclipse
    // consider a fully obscured sun for visibility percentages lower than the threshold
    if(this->sunVisibilityFactor.shadowFactor > this->sunVisibilityThreshold){
        // Determine whether the sun lies within the sensor's FOV
        if(this->nHat_B.dot(this->sHat_B) >= cos(this->fov)){
            this->isValid = true;
        }
        else{
            this->isValid = false;
        }
    }
    else{
        this->isValid = false;
    }
}


/**
 * @brief Corrupts the sensor's output by some error.
 * 
 * TODO: Explain the math.
 * It is the same if the error is applied in the sensor or the body frame because
 * for some vectors in frames a and b it is true that if x_b = x1_b + x2_b, then
 * x_a = R_ab*x1_b + R_ab*x2_b. If x2 is the vector added by the error, then its
 * magnitude will be the same in both frames because a rotation matrix does not
 * affect the magnitude of a vector.
 */
void FineSunSensor::addSensorErrors(){
    // Create a coordinate frame with sHat_B in its Z axis
    Eigen::Vector3d z_tmp = this->sHat_B;
    Eigen::Vector3d x_tmp(-z_tmp[1], z_tmp[0], 0.0);
        // handle the case where sHat_B is already along the Z axis
    if(x_tmp.norm() < 1e-8){
        x_tmp << 1.0, 0.0, 0.0;
    }
    Eigen::Vector3d y_tmp = z_tmp.cross(x_tmp);

    // Randomly sample 2 error angles based on the sensor's error figure
    std::uniform_real_distribution<double> unif(0.0, 1.0);

    double theta = this->distribution(this->random_engine);  // random tilt
    double phi = 2 * M_PI * unif(this->random_engine);  // random azimuthal rotation
    
    // Get the corrupt vector by rotating sHat_B
    this->sHat_B << cos(theta) * z_tmp + sin(theta)*(cos(phi) * x_tmp + sin(phi) * y_tmp);
}


void FineSunSensor::setFOV(double _fov){
    this->fov = _fov;
}


void FineSunSensor::setNoise1Sigma(double _noise1Sigma){
    this->noise1Sigma = _noise1Sigma;
    // also update the distribution member
    this->distribution = std::normal_distribution<double>(0.0, this->noise1Sigma);
}


void FineSunSensor::setSunVisibilityThreshold(double _sunVisibilityThreshold){
    this->sunVisibilityThreshold = _sunVisibilityThreshold;
}


/**
 * @brief Sets the rotation matrix from the sensor frame to the body frame
 * 
 * @param _dcm_BFSS 
 */
void FineSunSensor::setSensorFrame(Eigen::Matrix3d _dcm_BFSS){
    this->dcm_BFSS = _dcm_BFSS;
}


/**
 * @brief Sets the boresight heading vector in the sensor frame
 * 
 * @param _nHat_FSS 
 */
void FineSunSensor::setBoresightHeading(Eigen::Vector3d _nHat_FSS){
    this->nHat_FSS = _nHat_FSS;
}


/**
 * @brief The main module function
 * 
 * @param current_sim_nanos 
 */
void FineSunSensor::UpdateState(uint64_t current_sim_nanos){
    // Read input messages
    this->readInputMessages();

    // Find the sun heading vector
    this->findSunVector();

    // Consider eclipse and out-of-FOV occasions
    this->limitOutput();

    // Corrupt sensor measurement with noise
    this->addSensorErrors();

    // Write output message
    this->writeOutputMessage(current_sim_nanos);
}
