%module fineSunSensor
%{
   #include "fineSunSensor.h"
%}

%include "swig_conly_data.i"
%include "swig_eigen.i"
%include "std_vector.i"
%include "std_string.i"

%include "sys_model.i"
%include "fineSunSensor.h"

%include "architecture/msgPayloadDefC/EclipseMsgPayload.h"
%include "architecture/msgPayloadDefC/SpicePlanetStateMsgPayload.h"
%include "architecture/msgPayloadDefC/SCStatesMsgPayload.h"
%include "msgPayloadDefCpp/FSSRawDataMsgPayload.h"

struct EclipseMsg_C;
struct SpicePlanetStateMsg_C;
struct SCStatesMsg_C;
struct FSSRawDataMsg_C;

%pythoncode %{
import sys
protectAllClasses(sys.modules[__name__])
%}
