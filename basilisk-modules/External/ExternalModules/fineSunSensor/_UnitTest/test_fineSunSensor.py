import os
from copy import copy

import matplotlib.pyplot as plt
import numpy as np

from Basilisk import __path__

bskPath = __path__[0]
fileName = os.path.basename(os.path.splitext(__file__)[0])


# import simulation related support
from Basilisk.simulation import spacecraft, eclipse, coarseSunSensor
# general support file with common unit test functions
# import general simulation support files
from Basilisk.utilities import (SimulationBaseClass, macros, orbitalMotion,
                                simIncludeGravBody, unitTestSupport,
                                vizSupport)
from Basilisk.utilities import orbitalMotion as om
from Basilisk.ExternalModules import fineSunSensor
from Basilisk.architecture import messaging

from datetime import datetime
from datetime import timedelta

def run(sim_timestep:float, sim_time:float):

    # Convert simulation time and timestep to nanoseconds
    sim_time = macros.sec2nano(sim_time)
    sim_timestep = macros.sec2nano(sim_timestep)

    # Create simulation variable names
    dynamics_task_name = "Dynamics Task"
    main_process_name = "Main Process"

    # Create a sim module as an empty container
    sim = SimulationBaseClass.SimBaseClass()

    # Create the simulation process
    main_process = sim.CreateNewProcess(main_process_name)

    # Create the dynamics task and specify the integration update time
    main_process.addTask(sim.CreateNewTask(dynamics_task_name, sim_timestep))

    # Create a spacecraft object
    sc_object = spacecraft.Spacecraft()
    sc_object.ModelTag = "Phasma SC module"
    # Define the simulation inertia
    moi = [9.0, 0., 0.,
           0., 8.0, 0.,
           0., 0., 6.0]
    sc_object.hub.mHub = 4.5  # kg - spacecraft mass
    sc_object.hub.r_BcB_B = [[0.0], [0.0], [0.0]]  # m - position vector of body-fixed point B relative to CM
    sc_object.hub.IHubPntBc_B = unitTestSupport.np2EigenMatrix3d(moi)

    # Add the spacecraft object to the simulation process
    sim.AddModelToTask(dynamics_task_name, sc_object)

    # Set up gravitational forces
    # see the scenarioOrbitMultiBody.py to simulate multiple bodies using SPICE
    grav_factory = simIncludeGravBody.gravBodyFactory()
    grav_bodies = grav_factory.createBodies('earth', 'sun')
    grav_bodies['earth'].isCentralBody = True  # ensure this is the central grav. body
    grav_bodies['earth'].useSphericalHarmonicsGravityModel(bskPath + '/supportData/LocalGravData/GGM03S-J2-only.txt', 2)  # NOQA: E501
    mu = grav_bodies['earth'].mu
    # Connect gravitational bodies to the spacecraft object
    grav_factory.addBodiesTo(sc_object)

    # Next, the default SPICE support module is created and configured.  The first step is to store
    # the date and time of the start of the simulation.
    timeInitString = "2012 MAY 1 00:28:30.0"
    spiceTimeStringFormat = '%Y %B %d %H:%M:%S.%f'
    time_init = datetime.strptime(timeInitString, spiceTimeStringFormat)
    spice_object = grav_factory.createSpiceInterface(time=timeInitString, epochInMsg=True)
    spice_object.ModelTag = "SPICE module"
    spice_object.zeroBase = 'Earth'
    sim.AddModelToTask(dynamics_task_name, spice_object)

    # Set up a module to track the Sun position
    sun_position_msg = messaging.SpicePlanetStateMsgPayload()
    sun_position_msg.PositionVector = [0.0, -om.AU * 1000.0, 0.0]
    sun_msg = messaging.SpicePlanetStateMsg().write(sun_position_msg)

    planet_position_msg = messaging.SpicePlanetStateMsgPayload()
    planet_position_msg.PositionVector = [0., 0., 0.]
    planet_position_msg.PlanetName = 'earth'
    planet_position_msg.J20002Pfix = np.identity(3)
    planet_msg = messaging.SpicePlanetStateMsg().write(planet_position_msg)

    # Set up a module to compute eclipse
    eclipse_object = eclipse.Eclipse()
    eclipse_object.ModelTag = "Eclipse module"
    # eclipse_object.sunInMsg.subscribeTo(sun_msg)
    eclipse_object.sunInMsg.subscribeTo(spice_object.planetStateOutMsgs[1])
    eclipse_object.addSpacecraftToModel(sc_object.scStateOutMsg)
    eclipse_object.addPlanetToModel(planet_msg)
    sim.AddModelToTask(dynamics_task_name, eclipse_object)

    # Set up the fineSunSensor module
    fine_sun_sensor = fineSunSensor.FineSunSensor()
    fine_sun_sensor.ModelTag = "FSS module"
    fine_sun_sensor.setFOV(np.deg2rad(90))
    fine_sun_sensor.setNoise1Sigma(np.deg2rad(1.0))
    fine_sun_sensor.setSunVisibilityThreshold(0.7)
    dcm_BFSS = [1.0, 0.0, 0.0,
                0.0, 1.0, 0.0,
                0.0, 0.0, 1.0]
    fine_sun_sensor.setSensorFrame(unitTestSupport.np2EigenMatrix3d(dcm_BFSS))
    fine_sun_sensor.setBoresightHeading(unitTestSupport.np2EigenVectorXd([0.0, 0.0, 1.0]))
    fine_sun_sensor.eclipseInMsg.subscribeTo(eclipse_object.eclipseOutMsgs[0])
    fine_sun_sensor.sunInMsg.subscribeTo(spice_object.planetStateOutMsgs[1])
    # fine_sun_sensor.sunInMsg.subscribeTo(sun_msg)
    fine_sun_sensor.stateInMsg.subscribeTo(sc_object.scStateOutMsg)
    sim.AddModelToTask(dynamics_task_name, fine_sun_sensor)

    # Set up a CSS to test the FSS results
    CSS1 = coarseSunSensor.CoarseSunSensor()
    CSS1.ModelTag = "Coarse Sun Sensor module"
    CSS1.fov = 90. * macros.D2R
    CSS1.maxOutput = 1.0
    CSS1.nHat_B = np.array([0.0, 0.0, 1.0])
    CSS1.sunInMsg.subscribeTo(spice_object.planetStateOutMsgs[1])
    CSS1.sunEclipseInMsg.subscribeTo(eclipse_object.eclipseOutMsgs[0])
    CSS1.stateInMsg.subscribeTo(sc_object.scStateOutMsg)
    sim.AddModelToTask(dynamics_task_name, CSS1)

    # Set up the orbit using orbital elements
    oe = orbitalMotion.ClassicElements()
    rLEO = 7000. * 1000      # meters
    oe.a = rLEO
    oe.e = 0.0001
    oe.i = 33.3 * macros.D2R
    oe.Omega = 48.2 * macros.D2R
    oe.omega = 347.8 * macros.D2R
    oe.f = 85.3 * macros.D2R
    rN, vN = orbitalMotion.elem2rv(mu, oe)

    # Convert back to oe since some changes might have occured during
    # the oe-to-rv conversion (this is done in the basilisk-provided)
    # example, which mentions that "with circular or equatorial orbit,
    # some angles are arbitrary"
    oe = orbitalMotion.rv2elem(mu, rN, vN)

    # Set the spacecraft initial r-v
    sc_object.hub.r_CN_NInit = rN  # m   - r_BN_N
    sc_object.hub.v_CN_NInit = vN  # m/s - v_BN_N

    # Set the initial spacecraft attitude
    sc_object.hub.sigma_BNInit = [[0.0], [0.0], [0.0]]  # sigma_BN_B
    sc_object.hub.omega_BN_BInit = [[0.001], [0.0], [0.0]]  # rad/s - omega_BN_B

    # set the simulation time
    n = np.sqrt(mu / oe.a / oe.a / oe.a)
    P = 2. * np.pi / n

    # create a logging task object of the spacecraft output message at the desired down sampling ratio
    data_rec = sc_object.scStateOutMsg.recorder()
    data_rec.ModelTag = "Spacecraft state recorder"
    sim.AddModelToTask(dynamics_task_name, data_rec)

    fss_rec = fine_sun_sensor.fssDataOutMsg.recorder()
    fss_rec.ModelTag = "FSS recorder"
    sim.AddModelToTask(dynamics_task_name, fss_rec)

    css_rec_1 = CSS1.cssDataOutMsg.recorder()
    css_rec_1.ModelTag = "CSS recorder 1"
    sim.AddModelToTask(dynamics_task_name, css_rec_1)

    css_rec_2 = CSS1.logger("sHat_B")
    css_rec_2.ModelTag = "CSS recorder 2"
    sim.AddModelToTask(dynamics_task_name, css_rec_2)

    # Vizualize simulation in Vizard
    # viz = vizSupport.enableUnityVisualization(sim, dynamics_task_name,
    #                                           sc_object,
    #                                           # saveFile=__file__
    #                                           # liveStream=True
    #                                           )

    # Initialize simulation
    sim.InitializeSimulation()

    # Show a module diagram
    # sim.ShowExecutionFigure()

    # Show a simulation progress bar in the terminal
    sim.SetProgressBar(True)

    # Configure a simulation stop time and execute the simulation run
    sim.ConfigureStopTime(sim_time)
    sim.ExecuteSimulation()

    # Retrieve the logged data
    pos_data = data_rec.r_BN_N
    vel_data = data_rec.v_BN_N
    attitude_data = data_rec.sigma_BN
    ang_vel_data = data_rec.omega_BN_B

    sHat_B_data = fss_rec.sHat_B
    fss_valid_data = fss_rec.isValid

    css_output = css_rec_1.OutputData
    sHat_B_data_css = css_rec_2.sHat_B

    return pos_data, vel_data, attitude_data, ang_vel_data, sHat_B_data, fss_valid_data, css_output, sHat_B_data_css


if __name__ == "__main__":
    sim_timestep = 1
    sim_time = 11600
    pos_data, vel_data, attitude_data, ang_vel_data, sHat_B, fss_valid, css_output, sHat_B_css = run(sim_timestep, sim_time)

    time = np.arange(0, sim_time+sim_timestep, sim_timestep)
    # fig, ax = plt.subplots()
    # l0, = ax.plot(time, ang_vel_data[:, 0], label="sigma 1")
    # l1, = ax.plot(time, ang_vel_data[:, 1], label="sigma 2")
    # l2, = ax.plot(time, ang_vel_data[:, 2], label="sigma 3")
    # ax.legend(handles=[l0, l1, l2])

    fig1, (ax1, ax2, ax3) = plt.subplots(3, 1)
    l1, = ax1.plot(time, sHat_B[:, 0], label="sHat_B-x FSS")
    l2, = ax1.plot(time, sHat_B_css[:, 0], label="sHat_B-x true")
    ax1.legend(handles=[l1, l2])

    l3, = ax2.plot(time, sHat_B[:, 1], label="sHat_B-y FSS")
    l4, = ax2.plot(time, sHat_B_css[:, 1], label="sHat_B-y true")
    ax2.legend(handles=[l3, l4])

    l5, = ax3.plot(time, sHat_B[:, 2], label="sHat_B-z FSS")
    l6, = ax3.plot(time, sHat_B_css[:, 2], label="sHat_B-z true")
    ax3.legend(handles=[l5, l6])
    fig1.suptitle("Sun heading vector in the body frame - 1deg 1sigma noise")

    fig2, ax4 = plt.subplots()
    sun_visibility_threshold = 0.7
    l1, = ax4.plot(time, css_output, label="scaled CSS output")  # plot normalized output in the [0-1] range
    l2, = ax4.plot(time, fss_valid, label="FSS valid flag")
    # ax4.axhline(y=sun_visibility_threshold, linestyle="--", c="r")
    ax4.legend(handles=[l1, l2])

    plt.show()
